<?php 

/* Include all the classes */ 
	require_once(__DIR__."/class/pDraw.class.php"); 
	require_once(__DIR__."/class/pImage.class.php"); 
	require_once(__DIR__."/class/pData.class.php");

/* Create your dataset object */ 
$MyData = new pData(); 
/* Add data in your dataset */ 
$pro = $_GET['ma'];
$actual = $_GET['act'];

$xyz = $_GET['pn'];
foreach($pro as $pt){
	if($pt != 'VOID') $pt = intval($pt);
	$MyData->addPoints($pt, "promise");
	
}

foreach($actual as $act){
	if($act != 'VOID') $act = intval($act);
	$MyData->addPoints($act, "actual");
}

$MyData->setSerieOnAxis("promise",0);
$MyData->setSerieOnAxis("actual",0);
$MyData->setAxisName(0,"Promised vs Actual");
//$MyData->setAxisName(1,"Actual");

foreach($xyz as $pxz){
	$postr = (string)$pxz;
	$MyData->addPoints($postr,'ponumbers');
}
$MyData->setSerieDescription("ponumbers","PO Numbers");
$MyData->setAbscissa("ponumbers");

/* Create a pChart object and associate your dataset */ 
$myPicture = new pImage(700,230,$MyData);
/* Choose a nice font */
$myPicture->setFontProperties(array("FontName"=>"fonts/Forgotte.ttf","FontSize"=>11));

/* Define the boundaries of the graph area */
$myPicture->setGraphArea(60,40,670,190);

/* Draw the scale, keep everything automatic */ 
$myPicture->drawScale();

/* Draw the scale, keep everything automatic */ 
$myPicture->drawSplineChart();

/* Build the PNG file and send it to the web browser */ 
$myPicture->Render("performance.png"); 
?>






