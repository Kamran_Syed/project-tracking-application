<?php 
echo "0<br/>";

/* Include all the classes */ 
	require_once(__DIR__."/class/pDraw.class.php"); 
	require_once(__DIR__."/class/pImage.class.php"); 
	require_once(__DIR__."/class/pData.class.php");
echo "1<br/>";
/* Create your dataset object */ 
$myData = new pData(); 
echo "2<br/>";
/* Add data in your dataset */ 
$pro = $_GET['ma'];
$actual = $_GET['act'];
$p_no = $_GET['pn'];
foreach($pro as $pt){
	if($pt != 'VOID') $pt = intval($pt);
	$myData->addPoints($pt, "promise");
	
}
foreach($actual as $act){
	if($act != 'VOID') $act = intval($act);
	$myData->addPoints($act, "actual");
}
echo "3<br/>";
$MyData->setSerieOnAxis("promise",0);
$MyData->setSerieOnAxis("actual",1);
$MyData->setAxisName(0,"promise");
$MyData->setAxisName(1,"actual");
foreach($p_no as $ppn){
	$MyData->addPoints($ppn),"Po Numbers");
}
$MyData->setSerieDescription("Labels","Po Numbers");
$MyData->setAbscissa("Labels");
/* Create a pChart object and associate your dataset */ 
$myPicture = new pImage(700,230,$myData);
/* Choose a nice font */
$myPicture->setFontProperties(array("FontName"=>"fonts/Forgotte.ttf","FontSize"=>11));
echo "4<br/>";
/* Define the boundaries of the graph area */
$myPicture->setGraphArea(60,40,670,190);

/* Draw the scale, keep everything automatic */ 
$myPicture->drawScale();

/* Draw the scale, keep everything automatic */ 
$myPicture->drawSplineChart();

/* Build the PNG file and send it to the web browser */ 
$myPicture->Render("basic.png"); 
?>
<img src='basic.png'/>

