<?php
/*
Plugin Name: Project tracking application
Plugin URI: 
Description: Project tracking application
Version: 1.2
Author: AgileSolutionspk
Author URI: http://agilesolutionspk.com/
*/


global $product;
if ( !class_exists( 'Agile_Project_tracking_application' )){  
	class Agile_Project_tracking_application{
		
		function logout_redirect(){
			?>
			<script>window.location.href="<?php echo home_url();?>"</script>
			<?php
			
		}
		function include_css(){
		?>
			<link rel='stylesheet'   href='<?php echo plugins_url('project-tracking/css/agile-bootstrap.css');?>' type='text/css' media='all' />
			<script type="text/javascript" src="<?php echo plugins_url('project-tracking/js/js-agile-bootstrap.js');?>"></script>
		<?php
		}
		function front_scripts(){
			wp_enqueue_script('js-jquery_graph_front',plugins_url('js/jquery_graph.js', __FILE__));
			wp_enqueue_script('js-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__));
			wp_enqueue_style('bootstrap-css',plugins_url('css/agile-bootstrap.css', __FILE__));
		}
		function front_end(){
			if(! session_id()) session_start();
			ob_start();
			
		}
		function install(){
			global $wpdb;
			
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			$sql="
				CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."joe_eboje (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `client_name` varchar(30) NOT NULL,
				  `po_number` varchar(30) NOT NULL,
				  `field` varchar(30) NOT NULL,
				  `invoice_number` varchar(30) NOT NULL,
				  `invoice_status` varchar(30) NOT NULL,
				  `supplier_name` varchar(30) NOT NULL,
				  `description` varchar(30) NOT NULL,
				  `hazardous` varchar(30) NOT NULL,
				  `origin` varchar(30) NOT NULL,
				  `terms` varchar(30) NOT NULL,
				  `method` varchar(30) NOT NULL,
				  `no_of_pkg` varchar(30) NOT NULL,
				  `weight` varchar(30) NOT NULL,
				  `volume` varchar(30) NOT NULL,
				  `charge_weight` varchar(30) NOT NULL,
				  `pickup_req` date NOT NULL,
				  `instruc_req` varchar(30) NOT NULL,
				  `po_receipt` date NOT NULL,
				  `actioned` date NOT NULL,
				  `available` date NOT NULL,
				  `collection` date NOT NULL,
				  `ship` date NOT NULL,
				  `b_l` date NOT NULL,
				  `voyage` date NOT NULL,
				  `pol` date NOT NULL,
				  `eta` date NOT NULL,
				  `first_eta` date NOT NULL,
				  `current_date` date NOT NULL,
				  `comments` text NOT NULL,
				  `ets` date NOT NULL,
				  `atd` date NOT NULL,
				  `pre_alert` date NOT NULL,
				  `sea_line` varchar(30) NOT NULL,
				  `attach_id` varchar(255) NOT NULL,
				  `new_eta` date NOT NULL,
				  `new_ata` date NOT NULL,
				  `new_green_req` date NOT NULL,
				  `new_green_aproved` date NOT NULL,
				  `new_no_days` varchar(30) NOT NULL,
				  `freight_mawb` varchar(30) NOT NULL,
				  `freight_hawb` varchar(30) NOT NULL,
				  `freight_airport` varchar(30) NOT NULL,
				  `freight_etd` date NOT NULL,
				  `freight_atd` date NOT NULL,
				  `freight_eta` date NOT NULL,
				  `freight_prealert` date NOT NULL,
				  `freight_airline` varchar(30) NOT NULL,
				  `freight_green_req` date NOT NULL,
				  `freight_light_apr` date NOT NULL,
				  `freight_day_green` varchar(30) NOT NULL,
				  `application_for_pre_release` date NOT NULL,
				  `pre_release_approved` date NOT NULL,
				  `custom_clearance` date NOT NULL,
				  `delivery` date NOT NULL,
				  `delivery_note_number` VARCHAR(30) NOT NULL,
				  `req_delivery_date` date NOT NULL,
				  `act_delivery_date` date NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
				";
			dbDelta($sql);
			$sql_log = "CREATE TABLE IF NOT EXISTS`".$wpdb->prefix."agile_history_log` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `date` datetime NOT NULL,
			  `comments` text(5000) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1;";
			dbDelta($sql_log);
			// add role when user create admin for tracking system 
				 add_role('joe_eboje', 'Joe-Eboje', array('read' => true,'joe_eboje' => true,));
		}
		function my_login_redirect( $redirect_to, $request, $user ) {
			//is there a user to check?
			global $user;
			if ( isset( $user->roles ) && is_array( $user->roles ) ) {
				//check for admins
				if ( in_array( 'joe_eboje', $user->roles ) ) {
					// redirect them to joe dashboard
					$page_link = get_permalink( get_page_by_title( 'Dashboard' ) );
					return $page_link;
				}
			}
			return $redirect_to;
 		}
		
		function tracking_application(){
			global $wpdb;
			
			if(isset($_GET['report'])){
				if($_GET['report'] == 'pdf'){
					$this->generate_report_pdf_frontend();
					exit;
				}
			}
			if(isset($_POST['track_performance'])){
				$client_name = $_POST['client_name_track'];
				$result = $this->select_project_status_data_for_client($client_name);
				if(count($result) < 1){
					?>
					<script>
					   jQuery(document).ready(function () {
							setTimeout(function(){window.location.href= '<?php echo  home_url('wp-login.php');?>'}, 1800000);
						});	
					</script>
					
						<script>alert("No Data Available for <?php echo $client_name;?>");</script>
					<?php
					return;
				}
				$array = array();
				foreach($result as $r){
					$po_nmr = $r->po_number;
					$p_nmr[] = $po_nmr;
					$create_date = strtotime($r->current_date);
					$your_date = strtotime($r->act_delivery_date);
					$datediff =  $your_date - $create_date;
					$act_day =  floor($datediff/(60*60*24));
					$old_date = strtotime($r->req_delivery_date);
					$date =  $old_date - $create_date;
					$promise_day =  floor($date/(60*60*24));
					$array[$n]= $act_day; 
					$pro_day[] = $promise_day ;
					$actual_day[] = $act_day ;
					$abc[] = "['$po_nmr', $promise_day , $act_day],";
				}
				$x = 'ma[]=VOID&';
				$act = 'act[]=VOID&';
				foreach($pro_day as $y){
					$x = $x.'ma[]='.$y.'&';
				} 

				foreach($actual_day as $ac){
					$act = $act.'act[]='.$ac.'&';
				}
				foreach($p_nmr as $pn){
					$pno = $pno.'pn[]='.$pn.'&';
				}
				
				$url_pl = plugins_url();
				$url =  $url_pl.'/project-tracking/pChart2.1.4/gdlib.php?'.$x.$act.$pno;
				$filename = file_get_contents($url);
				$img_graph = $url_pl.'/project-tracking/pChart2.1.4/performance.png';
				if(!empty($filename)){
					?>
						<img src = "<?php echo $img_graph; ?>" />
					<?php
					 $name = ABSPATH.'/wp-content/plugins/project-tracking/pChart2.1.4/performance.png';
					echo $name;
					if (file_exists($name)){
						ob_end_clean();
						header('Content-Description: File Transfer');
						header('Content-Type: application/octet-stream');
						header('Content-Disposition: attachment; filename='.basename($name));
						header('Expires: 0');
						header('Cache-Control: must-revalidate');
						header('Pragma: public');
						header('Content-Length: ' . filesize($name));
						readfile($name);
						exit;
					}  
				}
				?>
				<div id = "graph_show">
					<script type="text/javascript">
					 /* google.load("visualization", "1", {packages:["corechart"]});
					  google.setOnLoadCallback(drawChart);
					  function drawChart() {
						var data = google.visualization.arrayToDataTable([
						  ['PO', 'Promised', 'Actual'],<?php 
							$arrcnt = count($abc);
							$abc[$arrcnt -1] = rtrim($abc[$arrcnt -1],",");
							foreach($abc as $a){
								echo $a;
							}
						  ?>]);
						var options = {
						  title: 'Company Performance'
						};

						var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
						chart.draw(data, options);
					  }*/
					  
					  
					</script>
					<div id="chart_div" style=" clear:left; float:left; margin-top:1em;"></div>
				</div>
				<?php
				
				return;
			}
			if(isset($_POST['track_project'])){
				$po_number = $_POST['po_num'];
				$result = $this->select_project_status_data($po_number);
					$attachment = $result[0]->attach_id;
					if($attachment){
						$attachments = unserialize ( $attachment );
						foreach($attachments as $attachment_id){
							$attch_url = wp_get_attachment_url( $attachment_id );
							$x = basename($attch_url);
						}
					}
				if($result){
					$shipping_method = $result[0]->method;
						?>
						<script>
							jQuery( document ).ready(function() {
								if('air' == '<?php echo $result[0]->method; ?>'){
									jQuery("#aspk_shipping").hide();
								}
								else{
									jQuery("#aspk_air_shipping").hide();
								}
							});
						</script>
						<?php
					ob_start();
					?>
					<div class="tw-bs container" id = "show_track">
						<div class="row" style = " clear:left;">
							<div class="col-md-6" style="width:10em;"><span style="font-weight: 400;color: #000069;font-size: 18px;">Client Name</span></div>
							<div class="col-md-6"><?php echo $result[0]->client_name; ?></div>
						</div>
						<div class="row" style = " clear:left;">
							<div class="col-md-6" style="width:10em;"><span style="font-weight: 400;color: #000069;font-size: 18px;">PO Number</span></div>
							<div class="col-md-6" ><?php echo $result[0]->po_number; ?></div>
						</div>
						<div class="row" style = " clear:left;">
							<div class="col-md-12" style="margin-top:1em;"></div>
						</div>
						<!--Removed <font size="3">Font Size-->
						<div  style="float:left;clear:left;border: 2px solid grey; border-radius: 25px;width:44em;padding: 1em;">
							<div class = "row"><div class = "col-md-12"><h3><b>General Information</b></h3><hr style = "color:grey;height:1px;border:none;color:#333;background-color:#333;"/></div></div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="background-color:rgb(231, 227, 235);"><b>Field</b></div>
								<div class="col-md-6" style="background-color:rgb(231, 227, 235);"><?php echo $result[0]->field; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Invoice No</b></div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->invoice_number;?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Supplier</b></div>
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->supplier_name; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Descprtion</b></div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->description; ?></div>
							</div>
							<div class="row" style = "clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Hazardous</b></div>
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->hazardous; ?></div>
							</div>
							<div class="row" style = "clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Origin</b></div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->origin; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Terms</b></div>
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->terms; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Method</b></div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->method; ?></div>
							</div>
						</div>
						<div class="row" style = " clear:left;">
							<div class="col-md-12" style="margin-top:1em;"></div>
						</div>
						<div  style="float:left;clear:left;border: 2px solid grey; border-radius: 25px;width:44em;padding: 1em;">
							<div class="row" style = "clear:left;">
								<div class="col-md-12" ><h3><b>Packing Lists</b></h3><hr style="color:grey;height:1px;border:none;color:#333;background-color:#333;" /></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>No of Packages</b></div>
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->no_of_pkg; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Weight(KG)</b></div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->weight; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Volume(CBM)</b></div>
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->volume; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Charge_weight(F/T):</b> </div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->charge_weight; ?></div>
							</div>
						</div>
						<div class="row" style = " clear:left;">
							<div class="col-md-12" style="margin-top:1em;"></div>
						</div>
						<div  style="float:left;clear:left;border: 2px solid grey; border-radius: 25px;width:44em;padding: 1em;">
							<div class="row" style = " clear:left;">
								<div class="col-md-12" ><h3><b>Inland Freight</b></h3><hr style = "color:grey;height:1px;border:none;color:#333;background-color:#333;"/></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Pickup Request</b></div>
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->pickup_req; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Instruct Request</b></div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->instruc_req; ?></div>
							</div>
							<div class="row" style = "clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>PO Receipt:</b></div>
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->po_receipt; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Actioned</b></div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->actioned; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Available</b></div>
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->available; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Collection</b></div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->collection; ?></div>
							</div>  
						</div>
						<div class="row" style = " clear:left;">
							<div class="col-md-12" style="margin-top:1em;"></div>
						</div>
						<? if($result[0]->method == 'sea'){ ?>
						<div  style="float:left;clear:left;border: 2px solid grey; border-radius: 25px;width:44em;padding: 1em;" id = "aspk_shipping">
							<div class="row" style = " clear:left;">
								<div class="col-md-12" ><h3><b>Shipping Details</b></h3><hr style = "color:grey;height:1px;border:none;color:#333;background-color:#333;"/></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Ship</b></div>
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->ship; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>B L</b></div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->b_l; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Voyage</b></div>
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->voyage; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Port Of Loading</b></div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->pol; ?></div>
							</div>
												<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>ETA</b></div>
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->eta; ?></div>
							</div>
							<div class="row" style = " clear:left;">
									<div class="col-md-6" style="margin-top:1em;"><b>Pre Alert</b></div>
									<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->pre_alert; ?></div>
							</div>
							<div class="row" style = " clear:left;">
									<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Sea Line</b></div>
									<div class="col-md-6" style="margin-top:1em; background-color:rgb(231, 227, 235);"><?php echo $result[0]->sea_line; ?></div>
							</div>
							<div class="row" style = "clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>ATD</b></div>
								<div class="col-md-6" style="margin-top:1em;"><?php echo $result[0]->first_eta; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em; background-color:rgb(231, 227, 235);"><b>ETS</b></div>
								<div class="col-md-6" style="margin-top:1em; background-color:rgb(231, 227, 235);" ><?php echo $result[0]->ets; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em; background-color:rgb(231, 227, 235);"><b>Green Light requested</b></div>
								<div class="col-md-6" style="margin-top:1em; background-color:rgb(231, 227, 235);" ><?php echo $result[0]->new_green_req; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em; background-color:rgb(231, 227, 235);"><b>Green light approved</b></div>
								<div class="col-md-6" style="margin-top:1em; background-color:rgb(231, 227, 235);" ><?php echo $result[0]->new_green_aproved; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em; background-color:rgb(231, 227, 235);"><b>Number of days for Green Light</b></div>
								<div class="col-md-6" style="margin-top:1em; background-color:rgb(231, 227, 235);" ><?php echo $result[0]->new_no_days; ?></div>
							</div>
						</div>
						<?php } ?>
						<div class="row" ><div class = "col-md-12"></div></div>
						<? if($result[0]->method == 'air'){ ?>
						<div  style="float:left;clear:left;border: 2px solid grey; border-radius: 25px;width:44em;padding: 1em;" id = "aspk_air_shipping">
							<div class="row" style = " clear:left;">
								<div class="col-md-12"><h3><b>Air Freight Details</b></h3><hr style = "color:grey;height:1px;border:none;color:#333;background-color:#333;"/></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>MAWB</b></div>
								<div class="col-md-6"  style="margin-top:1em;"><?php echo $result[0]->freight_mawb; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>HAWB</b></div>
								<div class="col-md-6"  style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->freight_hawb; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Airport</b></div>
								<div class="col-md-6"  style="margin-top:1em;"><?php echo $result[0]->freight_airport; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>ETD</b></div>
								<div class="col-md-6"  style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->freight_etd; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>ATD</b></div>
								<div class="col-md-6"  style="margin-top:1em;"><?php echo $result[0]->freight_eta; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>ETA</b></div>
								<div class="col-md-6"  style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->freight_eta; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Pre Alert</b></div>
								<div class="col-md-6"  style="margin-top:1em;"><?php echo $result[0]->freight_prealert; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Air Line</b></div>
								<div class="col-md-6"  style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->freight_airline; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Green Light Requested</b></div>
								<div class="col-md-6"  style="margin-top:1em;"><?php echo $result[0]->freight_green_req; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Green Light Approved</b></div>
								<div class="col-md-6"  style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->freight_light_apr; ?></div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-6" style="margin-top:1em;"><b>Number Of Days Of Green Light</b></div>
								<div class="col-md-6"  style="margin-top:1em;"><?php echo $result[0]->freight_day_green; ?></div>
							</div>
						</div>
						<?php } ?>
						<div class = "row"><div class = "col-md-12"></div></div>
							<div style="float:left;clear:left;border: 2px solid grey; border-radius: 25px;width:44em;padding: 1em;">
								<div class="row" style = " clear:left;">
									<div class="col-md-12"><h3><b>Customs Section</b></h3><hr style = "color:grey;height:1px;border:none;color:#333;background-color:#333;"/></div>
								</div>
								<div class="row" style = " clear:left;">
									<div class="col-md-6" style="margin-top:1em;"><b>Application For Pre Release</b></div>
									<div class="col-md-6"  style="margin-top:1em;"><?php echo $result[0]->application_for_pre_release; ?></div>
								</div>
								<div class="row" style = " clear:left;">
									<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Pre Release Approved</b></div>
									<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->pre_release_approved; ?></div>
								</div>
								<div class="row" style = " clear:left;">
									<div class="col-md-6" style="margin-top:1em;"><b>Customs Clearance</b></div>
									<div class="col-md-6" style="margin-top:1em;" ><?php echo $result[0]->custom_clearance; ?></div>
								</div>
								<div class="row" style = " clear:left;">
									<div class="col-md-6" style="margin-top:1em;background-color:rgb(231, 227, 235);"><b>Delivery</b></div>
									<div class="col-md-6"  style="margin-top:1em;background-color:rgb(231, 227, 235);"><?php echo $result[0]->delivery; ?></div>
								</div>
								<div class="row" style = " clear:left;">
									<div class="col-md-6" style="margin-top:1em;"><b>Delivery Note Number</b></div>
									<div class="col-md-6" style="margin-top:1em;" ><?php echo $result[0]->delivery_note_number; ?></div>
								</div>
							</div>
	
						<div class="row" style = " clear:left;">
							<div class="col-md-4" style="margin-top:1em;"><b>Last Updated</b></div>
							<div class="col-md-8" style="margin-top:1em;"><?php echo $result[0]->current_date; ?></div>
						</div>
						<div class="row" style = " clear:left;">
							<div class="col-md-4" style="margin-top:1em;"><b>Comments</b></div>
							<div class="col-md-8" style="margin-top:1em;"><?php echo $result[0]->comments; ?></div>
						</div>
						<?php
							$rpt = ob_get_flush();
							$_SESSION['aspk_tracking_report'] = $rpt;
							
						if($attch_url){
							$attch_count = get_post_meta($result[0]->po_number, '_po_cont_dwnld',true);
							if($attch_count != 1){
						?>
						<div class="row" style = " clear:left;">
							<div class="col-md-4" style="margin-top:1em;"></div>
							<div class="col-md-8" style="margin-top:1em;">
								<a href = "<?php echo $attch_url; ?>"><input type="button" class="btn btn-primary" value ="Download attachment" id="buton" onclick = "download_file_restrict();" title ="<?php echo $x;?>"></a>
							</div>
						</div>
						<?php } ?>
						<!--</font>-->
						<!--<script>							
							function download_file_restrict(){
							<?php 
									//global $wpdb;
									
									//$sql= "UPDATE {$wpdb->prefix}joe_eboje SET attach_id='' WHERE po_number={$result[0]->po_number}";
									//$wpdb->query($sql);
									//update_post_meta($result[0]->po_number, '_po_cont_dwnld', 1);
								?>
								jQuery('#buton').hide();
							}
						</script>!-->
						
						<?php } ?>
						
					</div>
					<div class = "tw-bs container">
						<div class="row" style = " clear:left;">
							<div class="col-md-4" >&nbsp;</div>
							<?php
								global $post;
								$link = get_permalink()."?report=pdf";
							?>
							<div class="col-md-6" ><a href = "<? echo $link;?>" target="_blank" ><input type="button"  style = "width: 12.5em;" class="btn btn-primary" value ="Generate Report"></div>
						</div>
					</div>
					
					<?php
					
					return;
				}else{
				?>
					<div style = "float:left; clear:left; background-color:white;"> <b>No Record Found:</b></div>
				<?php
				}
			}
			 if(isset($_POST['email_submit'])){
					$email_update = $_POST['email_lost'];
					if(! $email_update){
						$user_id = get_current_user_id();
						$users = get_user_by('id',$user_id);
						$email_update = $users->data->user_email;
						$password = $_POST['password_reg'];
					}	
					$user = get_user_by('email',$email_update);
					if($user){
						//check role if invalid return
						$joe_eboje = get_role( 'joe_eboje' );
						if($joe_eboje == true){
							if(empty($password)){
								$password = $this->rand_string(8);
							}
							wp_set_password( $password, $user->ID );
							$header = 'From: Joe-Eboje Ltd <noreply@joe-eboje.com>\r\n';
							$email = wp_mail($email_update,'Password reset',$password,$header);
							
							?><div style ="float:left; clear:left; background-color:white;">Password has been reset, please check your email</div><?php
						}
					}else{
						 ?>
					<div style="clear:left;float:left;background-color:white;">
					<?php echo "Sorry cannot change password"; ?>
					</div>
					<?php
					}
						
				 }
			if (  is_user_logged_in() ) {
				$user_id = get_current_user_id();
				$users_get = get_user_by('id',$user_id);
				$users_name = $users_get->data->user_nicename;
				?>
				<head>
				<style>
					.p:hover {
						background-color: #000069;
						font-size: 18px;
						font-weight: 400;
						color:white;
					}
					.p{
						padding: 3px;
					}
					#font_class{
						font-family:Trebuchet MS,Arial,Serif; 
						font-size:16px; 
						font-weight: 900;
					}
				</style>
				</head>
				<div style = "float:left; clear:left;"> 
					<div  onclick = "reset_pass();" style=" width: 12; float:left; margin-bottom:1em; margin-left:1em;">
						<div class="p" style="text-align:center;border-radius:2px">
							<font id="font_class">Change Password</font>
						</div>
					</div>
					<div onclick = "trck_proj();"  style=" width: 12em; float:left; margin-bottom:1em; margin-left:1em;  ">
						<div class="p" style="text-align:center;border-radius:2px">
							<font id = "font_class" >Track Project</font>
						</div>
					</div>
					<!--<div onclick = "performance_pass();"style=" width: 12em; float:left; margin-bottom:1em; margin-left:1em; ">
						<div class="p" style="text-align:center;border-radius:2px">
							<font id="font_class" >Performance</font>
						</div>
					</div>-->
				</div>
				<script>
					function reset_pass(){
						jQuery('#email_show_reset').show();
						jQuery('#track_project').hide();
						jQuery('#show_track').hide();
						jQuery('#graph_show').hide();
						jQuery('#track_preformance').hide();
					}
					function performance_pass(){
						jQuery('#email_show_reset').hide();
						jQuery('#track_project').hide();
						jQuery('#show_track').hide();
						jQuery('#track_preformance').show();
						jQuery('#graph_show').show();
					}
					function trck_proj(){
						jQuery('#email_show_reset').hide();
						jQuery('#track_project').show();
						jQuery('#track_preformance').hide();
						jQuery('#graph_show').hide();
					}
				</script>
				<div style="float:left;clear:left; display:none;" id="email_show_reset">
					<form method="post" action="">
					<script>
								function checkPasswordMatch() {
									var password = jQuery("#txtNewPassword").val();
									var confirmPassword = jQuery("#txtConfirmPassword").val();

									if (password != confirmPassword){
										jQuery("#divCheckPasswordMatch").html("Passwords do not match!");
										jQuery("#btn_register").attr("type","hidden");
									
									}else{
										jQuery("#divCheckPasswordMatch").html("Passwords match.");
										jQuery("#btn_register").attr("type","submit");
									}
							}

									jQuery(document).ready(function () {
								jQuery("#txtConfirmPassword").keyup(checkPasswordMatch);
							});
					</script>
					<div class="tw-bs container">
						<div class="row" style="clear:left;">
							<div class="col-md-2" style="margin-top:1em;">
								<b>New Password:</b>
							</div>
							<div class="col-md-10" style="margin-top:1em;">
								<input type="password" name="password_reg" value="" required id="txtNewPassword" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-2" style="margin-top:1em;">
								<b>Confirm Password:</b>
							</div>
							<div class="col-md-10" style="margin-top:1em;">
								<input type="password" name="password_reg_conf" value="" required  id="txtConfirmPassword" onChange="checkPasswordMatch();" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-12"  id="divCheckPasswordMatch";>
								&nbsp; 
							</div>
						</div>
						<div  class="row" style="clear:left;">
							<div class="col-md-2" style="margin-top:1em;"></div>
							<div class="col-md-10" style="margin-top:1em;">
								<input type="submit" name="email_submit" value="Change Password" id="btn_register"/> 
							</div>
						</div>
					</div>
						
					</form>
				</div>
				<!-- track project -->
				<div style="float:left;clear:left; display:none;" id="track_project">
					<form method="post" action="">
						<div  style="float:left;clear:left; margin-top:1em;" >
								Po Number:<input type="text" name="po_num" value="<?php $po_number; ?>" />
						</div>
						<div  style=" float:left;clear:left; margin-top:1em;">
							<input type="submit" name = "track_project" value="Track Project" />
						</div>
					</form>
					
				</div>
				<!-- Performance -->
				<div style="float:left;clear:left; display:none;" id="track_preformance">
					<form method="post" action="">
						<div  style="float:left;clear:left; margin-top:1em;" >
								Client Name:<input type="text" readonly name="client_name_track" value="<?php echo $users_name; ?>" />
						</div>
						<div  style=" float:left;clear:left; margin-top:1em;">
							<input type="submit" name = "track_performance" value="Download Performance" />
						</div>
					</form>
				</div>
				<?php
			}
			 
				 ?>
			 <div style="clear:left;float:left" id ="login_form">
				<?php
				if ( ! is_user_logged_in() ) { // Display WordPress login form:
					$args = array(
						'redirect' => admin_url(), 
						'form_id' => 'loginform-custom',
						'label_username' => __( 'Username' ),
						'label_password' => __( 'Password' ),
						'label_remember' => __( 'Remember' ),
						'label_log_in' => __( 'Log In ' ),
						'remember' => true
					);
					wp_login_form( $args );
					?>
			</div>
			<div class="row" style="clear:left;">
				<div class="col-md-12" style="margin-top:1em;">
					<input type="checkbox" name="forget_password" value="" onclick="password_forget();" />Forgot your password
				</div>
			</div>
			<div style="float:left;clear:left; display:none;" id="email_show">
				<div class="row" style="clear:left;">
					<form method="post" action="">
						<div class="col-md-6" style="margin-top:1em;" >
							
								Email:<input type="email" name="email_lost" value="" />
						</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input type="submit" name="email_submit" value="Submit" />
						</div>
					</form>
				</div>
			</div>
			<script>
				function password_forget(){
					jQuery('#email_show').show();
					jQuery('#login_form').hide();
				}
			</script>
			<?php
			} else { // If logged in:
				wp_loginout( home_url() ); // Display "Log Out" link.
				/*echo " | ";
				wp_register('', ''); // Display "Site Admin" link.*/
			}
			
 		}
		function backend_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('js-tracking-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__)
			);
			wp_enqueue_script('js-tcal',plugins_url('js/tcal.js', __FILE__)
			);
			wp_enqueue_script('js-jquery_graph',plugins_url('js/jquery_graph.js', __FILE__)
			);
			wp_enqueue_style( 'agile-tigra-calender', plugins_url('css/tcal.css', __FILE__) );
			wp_enqueue_style( 'agile-tracking-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
		}
		function addAdminPage(){
			add_menu_page('Project Tracking', 'Project Tracking', 'read', 'aspk_product_tracking', array(&$this, 'test_tracking_aspk'));
			add_submenu_page('', 'Create Project','Create Project', 'read', 'aspk_tracking_view_mu', array(&$this, 'Create_Project'));
			add_submenu_page('', 'Update Project Status','Update Project Status', 'read', 'aspk_tracking_update_mu', array(&$this, 'delete_create_user'));
			add_submenu_page('', 'View Performance','View Performance', 'read', 'aspk_View_Performance', array(&$this, 'aspk_view_performance'));
			add_submenu_page('', 'Change Password','Change Password', 'read', 'aspk_change_password', array(&$this, 'aspk_change_password'));
			add_submenu_page('aspk_product_tracking', 'Show History','Show History', 'read', 'aspk_show_history', array(&$this, 'aspk_show_history_form'));
			add_submenu_page('', 'PDF Report','PDF Report', 'manage_options', 'aspk_pdf_report', array(&$this, 'generate_report_pdf_frontend'));
			add_submenu_page('', 'Graph Report Pdf','Graph Report Pdf', 'manage_options', 'aspk_graph_report_bar_verticle', array(&$this, 'get_verticle_bar_chart'));
		}
		function aspk_show_history_form(){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}agile_history_log";
			$result = $wpdb->get_results($sql);
			?>
			<script>
			   jQuery(document).ready(function () {
					setTimeout(function(){window.location.href= '<?php echo  home_url('wp-login.php');?>'}, 1800000);
				});	
			</script>
			
				<div class="tw-bs container">
					<div class = "row" style = "clear:left;">
						<div class = "col-md-2">Time</div>
						<div class = "col-md-10">Activity</div>
					</div>
					<?php foreach ($result as $r){?>
						<div class = "row" style = "clear:left;">
							<div class = "col-md-2"><?php echo $r->date;?></div>
							<div class = "col-md-10"><?php echo $r->comments;?></div>
						</div>
					<?php } ?>
				</div>
			<?php
			
		}
		function aspk_change_password(){
		  ?>
		   	<script>
			   jQuery(document).ready(function () {
					setTimeout(function(){window.location.href= '<?php echo  home_url('wp-login.php');?>'}, 1800000);
				});	
			</script>
		  
		  <?
			$user_id = $_GET['uid'];
			$user = get_user_by('id',$user_id);
			$email_update = $user->data->user_email;
			$password = $this->rand_string(8);
			wp_set_password( $password, $user_id );
			$header = 'From: Joe-Eboje Ltd <noreply@joe-eboje.com>\r\n';
			wp_mail($email_update,'Password reset',$password,$header);
			echo "<b>Password has been reset</b>";
		}
		
		function graph_to_from_date($graph_to_date, $graph_from_date, $graph_client){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}joe_eboje Where `current_date` BETWEEN '{$graph_to_date}' AND '{$graph_from_date}' AND `client_name` = '{$graph_client}'";
			return $wpdb->get_results($sql);	
		}
		
		function graph_get_data(){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}joe_eboje";
			return $wpdb->get_results($sql);	
		}
		
		function aspk_view_performance(){
			global $wpdb;
			?>
			<script>
			   jQuery(document).ready(function () {
					setTimeout(function(){window.location.href= '<?php echo  home_url('wp-login.php');?>'}, 1800000);
				});	
			</script>
			
			<head>
			<style>
				#p:hover {
					background-color: blue;
					font-size: 10px;
					font-weight: 300;
					color:white;
				}
			</style>
			</head>
			<?php
			$link = admin_url();
			$args = array(
				'blog_id'      => $GLOBALS['blog_id'],
				'role'         => 'joe_eboje',
				'meta_key'     => '',
				'meta_value'   => '',
				'meta_compare' => '',
				'meta_query'   => array(),
				'include'      => array(),
				'exclude'      => array(),
				'orderby'      => 'login',
				'order'        => 'ASC',
				'offset'       => '',
				'search'       => '',
				'number'       => '',
				'count_total'  => false,
				'fields'       => 'all',
				'who'          => ''
			 );
			$get_all_users = get_users( $args );
			//id = to_from_form handling
			if(isset($_POST['date_btn'])){
				$graph_to_date =  date( 'Y-m-d', strtotime( $_POST['to_date'] ) );
				$graph_from_date =  date( 'Y-m-d', strtotime( $_POST['from_date'] ) );
				$graph_client =   $_POST['client_name'];
				$result = $this->graph_to_from_date($graph_to_date, $graph_from_date, $graph_client);
				$array = array();
				foreach($result as $r){
					$po_nmr = $r->po_number;
					$create_date = strtotime($r->current_date);
					$your_date = strtotime($r->act_delivery_date);
					$datediff =  $your_date - $create_date;
					$act_day =  floor($datediff/(60*60*24));
					$old_date = strtotime($r->req_delivery_date);
					$date =  $old_date - $create_date;
					$promise_day =  floor($date/(60*60*24));
					$array[$n]= $act_day;
					$abc[] = "['$po_nmr', $promise_day , $act_day],";
				}
				?>
				<div class="tw-bs container">
					<script type="text/javascript">
						  google.load("visualization", "1", {packages:["corechart"]});
						  google.setOnLoadCallback(drawChart);
						  function drawChart() {
							var data = google.visualization.arrayToDataTable([
							  ['PO', 'Promised', 'Actual'],<?php 
								$arrcnt = count($abc);
								$abc[$arrcnt -1] = rtrim($abc[$arrcnt -1],",");
								foreach($abc as $a){
									echo $a;
								}
							  ?>]);
							
							
							var options = {
							  title: 'Company Performance'
							};

							var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
							chart.draw(data, options);
						  } 
					</script>
				<?php 
					if(empty($result)){
						echo "Data No Found. Please Refresh Page.";
					}else{
					?>
					<!--<div id="chart_div" style=" clear:left; float:left; margin-top:2em;"></div>-->
					</div>
					<?php 
					if(! session_id()) session_start();
						ob_start();
						?>
                      	<div class = "tw-bs container">
							<div class="row" style = " clear:left;">
								<div class="col-md-3" style="margin-top:1em;">Report Generated time:</div> 
								<div class="col-md-5" style="background:white;margin-top:1em;"> <?php 
									echo  date("Y-m-d H:i:s");
								 ?>	 
								</div>
							</div>
							<div class="row" style = " clear:left;">
								<div class="col-md-3" style="margin-top:1em; ">Report Show:</div>	
								<div class="col-md-2" style="background:white;margin-top:1em;"> <?php echo $graph_to_date;	?></div>
								<div class="col-md-1"  style="margin-top:1em;margin-top:1em; ">To</div>
								<div class="col-md-2" style="background:white;margin-top:1em;"><?php 
									echo $graph_from_date;
								 ?>	</div>
								
								
							</div>
								
						</div>
					
					<?
						
					try{
						require_once __DIR__ ."/libchart/demo/VerticalBarChartTest.php";
					}catch (Exception $e){
						print_r($e);
					}
					$rpt = ob_get_flush();
					$_SESSION['aspk_tracking_report'] = $rpt;
				} 
				?>
					<div class = "tw-bs container">
						<div class="row" style = " clear:left;">
							<div class="col-md-4" >&nbsp;</div>
							<?php
								global $post;
								$link = admin_url('admin.php?page=aspk_graph_report_bar_verticle&report=pdf');	
							?>
							<div class="col-md-6" ><a href = "<? echo $link;?>" target="_blank" ><input type="button"  style = "width: 12.5em;" class="btn btn-primary" value ="Get Report"></div>
						</div>
					</div>
				<?php
				$date_time = date("Y-m-d H:i:s");
				$create_user = 'View Performance';
				$sql = "insert into {$wpdb->prefix}agile_history_log (date,comments) values('{$date_time}','{$create_user}');";
				$wpdb->query($sql);	
				exit;
			} ?>
			<div class="tw-bs container">	
				<div style = "float:left; clear:left; border-top: 1px solid #e3e3e3; border-bottom: 1px solid #e3e3e3; margin-top:10px;">
					<ul>
						<a href = "<?php echo $link.'admin.php?page=aspk_product_tracking';?>" style="text-decoration:none;"> 
						 <li id = "p" style = "width:14em; float:left; padding:8px;font-size: 14px;"> <b>Manage Client Access </b></li>
						 </a>
							<a href = "<?php echo $link.'admin.php?page=aspk_tracking_view_mu';?>" style="text-decoration:none;"> 
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b> Create Project </b></li>
							</a>
							<a href = "<? echo $link.'admin.php?page=aspk_tracking_update_mu'; ?>" style="text-decoration:none;"> 
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b> Update Project Status </b></li>
							</a>
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b>View Performance </b></li>	</a>
					</ul>
				</div>
			</div>
			<div style = "margin-top:6em;">
				<form method = "POST" id = "to_from_form">   
					<select name="client_name" style = "float:left; clear:left;">
						<?php 
						foreach($get_all_users as $user){ ?>
						<option value = "<?php echo  $user->data->user_nicename; ?>">
							<?php echo  $user->data->user_nicename; ?>
						</option>
						<?php }
						?>
					</select>
					To: <input type = "date" class = "tcal" name = "to_date" />&nbsp;&nbsp;&nbsp;
					From: <input type = "date" class = "tcal" name = "from_date" /><br/><br/>
					<input type = "submit" name = "date_btn" value = "Go" class = "button button-primary" />
				</form>
			</div>
			
			
			<?
		}
		
		function generate_report_pdf_frontend(){
		  ?>
		   	<script>
			   jQuery(document).ready(function () {
					setTimeout(function(){window.location.href= '<?php echo  home_url('wp-login.php');?>'}, 1800000);
				});	
			</script>
		  <?

			require_once __DIR__ .'/tcpdf/config/tcpdf_config.php';
			require_once __DIR__ .'/tcpdf/tcpdf.php';
			 ob_end_clean(); 

			// create new PDF document
			
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('Agile Solutions');
			$pdf->SetTitle('Report');
			$pdf->SetSubject('Front End Report');
			$pdf->SetKeywords('Report');
			
			// set default header data
			//$pdf->SetHeaderData('Tracking Report (joe-eboje.com)');
			
			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			
			// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			
			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			
			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			
			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			
			// set font
			$pdf->SetFont('dejavusans', '', 10);
			//$pdf->SetFont("Times-Roman", "host", 0)

			// add a page
			$pdf->AddPage();
			
			if(! session_id()){
				?>
					<script>alert('Sorry, No report Available');</script>
				<?php
				return;
			}
			$html = $_SESSION['aspk_tracking_report'];
			$pdf->writeHTML($html, true, false, true, false, '');
			
			$pdf->lastPage();
			
			$pdf->Output('Tracking-Report.pdf', 'I');
		} 
		
		public function create_project_tab_form(){ //create project tab
			global $wpdb;
			
			if(isset($_POST['submit_btn'])){
				$client_name = $_POST['client_name'];
				$po_number = $_POST['po_number'];
				$field = $_POST['field'];
				$invoice_number = $_POST['invoice_number'];
				$invoice_status = $_POST['invoice_status'];
				$supplier_name = $_POST['supplier_name'];
				$description = $_POST['description'];
				$hazardous = $_POST['hazardous'];
				$origin = $_POST['origin'];
				$terms = $_POST['terms'];
				$method = $_POST['method'];
				$no_of_pkg = $_POST['no_of_pkg'];
				$weight = $_POST['weight'];
				$volume = $_POST['volume'];
				$charge_weight = $_POST['charge_weight'];
				if($_POST['pickup_req']){
					$pickup_req = new DateTime($_POST['pickup_req']);
					$pickup_req = $pickup_req->format('Y-m-d');
				}else{
					$pickup_req = $_POST['pickup_req'];
				}
				if($_POST['instruc_req']){
					$instruc_req = new DateTime($_POST['instruc_req']);
					$instruc_req = $instruc_req->format('Y-m-d');
				}else{
					$instruc_req = $_POST['instruc_req'];
				}
				if($_POST['po_receipt']){
					$po_receipt = new DateTime($_POST['po_receipt']);
					$po_receipt = $po_receipt->format('Y-m-d');
					
				}else{
					$po_receipt = $_POST['po_receipt'];
				}
				if($_POST['actioned']){
					$actioned = new DateTime( $_POST['actioned']  );
					$actioned = $actioned->format('Y-m-d');
				}else{
					$actioned = $_POST['actioned'];
				}
				if($_POST['available']){
					$available = new DateTime($_POST['available'] );
					$available = $available->format('Y-m-d');
				}else{
					$available = $_POST['available'];
				}
				if($_POST['collection']){
					$collection = new DateTime( $_POST['collection']  );
					$collection = $collection->format('Y-m-d');
				}else{
					$collection = $_POST['collection'];
				}
				if($_POST['ship']){
					$ship = new DateTime($_POST['ship'] );
					$ship = $ship->format('Y-m-d');
				}else{
					$ship = $_POST['ship'];
				}
				if($_POST['b_l']){
					$b_l = new DateTime($_POST['b_l']);
					$b_l = $b_l->format('Y-m-d');
				}else{
					$b_l = $_POST['b_l'];
				}
				if($_POST['voyage']){
					$voyage = new DateTime($_POST['voyage']) ;
					$voyage = $voyage->format('Y-m-d');
				}else{
					$voyage = $_POST['voyage'];
				}
				if($_POST['pol']){
					$pol  = new DateTime($_POST['pol'] );
					$pol = $pol->format('Y-m-d');
				}else{
					$pol = $_POST['pol'];
				}
				if($_POST['eta']){
					$eta = new DateTime( $_POST['eta']  );
					$eta = $eta->format('Y-m-d');
				}else{
					$eta = $_POST['eta'];
				}
				if($_POST['ets']){
					$ets = new DateTime($_POST['ets']  );
					$ets = $ets->format('Y-m-d');
				}else{
					$ets = $_POST['ets'];
				}
				if($_POST['atd']  ){
					$atd = new DateTime( $_POST['atd']  );
					$atd = $atd->format('Y-m-d');
				}else{
					$atd = $_POST['atd'];
				}
				if($_POST['pre_alert']){
					$pre_alert = new DateTime( $_POST['pre_alert']  );
					$pre_alert = $pre_alert->format('Y-m-d');
				}else{
					$pre_alert = $_POST['pre_alert'];
				}
				$sea_line = $_POST['sea_line'];
				if($_POST['n_eta']){
					$sea_n_eta = new DateTime( $_POST['n_eta'] );
					$sea_n_eta = $sea_n_eta->format('Y-m-d');
				}else{
					$sea_n_eta = $_POST['n_eta'];
				}
				if($_POST['n_ata']){
					$sea_n_ata = new DateTime( $_POST['n_ata'] ); 
					$sea_n_ata = $sea_n_ata->format('Y-m-d');
				}else{
					$sea_n_ata = $_POST['n_ata'];
				}
				if($_POST['n_g_l_r']){
					$sea_n_g_l_r = new DateTime( $_POST['n_g_l_r'] );
					$sea_n_g_l_r = $sea_n_g_l_r->format('Y-m-d');
				}else{
					$sea_n_g_l_r = $_POST['n_g_l_r'];
				}
				if($_POST['n_g_l_a']){
					$sea_n_g_l_a = new DateTime( $_POST['n_g_l_a'] );
					$sea_n_g_l_a = $sea_n_g_l_a ->format('Y-m-d');
				}else{
					$sea_n_g_l_a = $_POST['n_g_l_a'];
				}
				$sea_n_days_light = $_POST['n_days_light'];
				$n_mawb = $_POST['n_mawb'];
				$n_hawb = $_POST['n_hawb'];
				$n_airport = $_POST['n_airport'];
				if($_POST['n_freight_etd']){
					$n_freight_etd = new DateTime( $_POST['n_freight_etd'] );
					$n_freight_etd = $n_freight_etd ->format('Y-m-d');	
				}else{
					$n_freight_etd = $_POST['n_freight_etd'];
				}
				if($_POST['n_freight_atd']){
					$n_freight_atd = new DateTime( $_POST['n_freight_atd'] );
					$n_freight_atd = $n_freight_atd ->format('Y-m-d');
				}else{
					$n_freight_atd = $_POST['n_freight_atd'];
				}
				if($_POST['n_freight_eta']){
					$n_freight_eta = new DateTime( $_POST['n_freight_eta'] );
					$n_freight_eta = $n_freight_eta ->format('Y-m-d');	
				}else{
					$n_freight_eta = $_POST['n_freight_eta'];
				}
				if($_POST['n_freight_pre_alert']){
					$n_freight_pre_alert = new DateTime( $_POST['n_freight_pre_alert'] );
					$n_freight_pre_alert = $n_freight_pre_alert ->format('Y-m-d');
				}else{
					$n_freight_pre_alert = $_POST['n_freight_pre_alert'];
				}
				$n_airline = $_POST['n_airline'];
				if($_POST['n_freight_green_r']){
					$n_freight_green_r = new DateTime( $_POST['n_freight_green_r'] );
					$n_freight_green_r = $n_freight_green_r ->format('Y-m-d');
				}else{
					$n_freight_green_r = $_POST['n_freight_green_r'];
				}
				if($_POST['n_freight_green_a']){
					$n_freight_green_a = new DateTime( $_POST['n_freight_green_a'] );
					$n_freight_green_a = $n_freight_green_a ->format('Y-m-d');
				}else{
					$n_freight_green_a = $_POST['n_freight_green_a'];
				}
				$n_freight_days_light  = $_POST['n_freight_days_light'];
				if($_POST['app_pre_release']){
					$application_for_pre_release = new DateTime($_POST['app_pre_release']);
					$application_for_pre_release = $application_for_pre_release->format('Y-m-d');
				}else{
					$application_for_pre_release = $_POST['app_pre_release'];
				}
				if($_POST['pre_release_approved']){
					$pre_release_approval = new DateTime($_POST['pre_release_approved']);
					$pre_release_approval = $pre_release_approval->format('Y-m-d');
				}else{
					$pre_release_approval = $_POST['pre_release_approved'];
				}
				if($_POST['custom_clearance']){
					$custom_clearance = new DateTime($_POST['custom_clearance']);
					$custom_clearance = $custom_clearance->format('Y-m-d');
				}else{
					$custom_clearance = $_POST['custom_clearance'];
				}
				if($_POST['delivery']){
					$delivery = new DateTime($_POST['delivery']);
					$delivery = $delivery->format('Y-m-d');
				}else{
					$delivery = $_POST['delivery'];
				}
				
				$delivery_note_no = $_POST['delivery_note_no'];
				if($_POST['req_delivery_date']){
					$req_delivery_date = new DateTime($_POST['req_delivery_date']);
					$req_delivery_date = $req_delivery_date->format('Y-m-d');
				}else{
					$req_delivery_date = $_POST['req_delivery_date'];
				}
					if($_POST['act_delivery_date']){
						$act_delivery_date = new DateTime($_POST['act_delivery_date']);
						$act_delivery_date = $act_delivery_date->format('Y-m-d'); 
					}	
					else{
						$act_delivery_date = $_POST['act_delivery_date'];
					}
				$attach_id = $_POST['wp_multi_file_uploader_'];
				$attach_id = serialize( $attach_id ); 
				
				$ret = $this->insert_project_form_data( $client_name, $po_number, $field, $invoice_number, $invoice_status, $supplier_name, $description, $hazardous, $origin, $terms, $method, $no_of_pkg, $weight, $volume, $charge_weight, $pickup_req, $instruc_req, $po_receipt, $actioned, $available, $collection, $ship, $b_l, $voyage, $pol, $eta, $ets, $atd, $pre_alert, $sea_line,$attach_id ,$sea_n_eta,$sea_n_ata,$sea_n_g_l_r,$sea_n_g_l_a,$sea_n_days_light,$n_mawb,$n_hawb,$n_airport,$n_freight_etd,$n_freight_etd,$n_freight_atd,$n_freight_eta,$n_freight_pre_alert,$n_airline,$n_freight_green_r,$n_freight_green_a,$n_freight_days_light,$application_for_pre_release,$pre_release_approval,$custom_clearance,$delivery,$delivery_note_no,$req_delivery_date,$act_delivery_date); /*Update Query For Insert.*/
				if($ret){
					?>
						<script>alert('Project Created Successfully');</script>
					<?php
				}else{
					?>
						<script>alert('Something Went Wrong  <?php echo $wpdb->$last_error;?>');</script>
					<?php
				}
			}
			
			$args = array(
				'blog_id'      => $GLOBALS['blog_id'],
				'role'         => 'joe_eboje',
				'meta_key'     => '',
				'meta_value'   => '',
				'meta_compare' => '',
				'meta_query'   => array(),
				'include'      => array(),
				'exclude'      => array(),
				'orderby'      => 'login',
				'order'        => 'ASC',
				'offset'       => '',
				'search'       => '',
				'number'       => '',
				'count_total'  => false,
				'fields'       => 'all',
				'who'          => ''
			 );
			$get_all_users = get_users( $args );
			?>
			<div class="tw-bs container">
                <h3>Create Project Tab</h3>
                <form method="post" action="" id = "project_tab_form">
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Client Name: </div>
                        <div class="col-md-10" style="margin-top:1em;">
							<select name="client_name">
								<?php 
								foreach($get_all_users as $user){ ?>
								<option value = "<?php echo  $user->data->user_nicename; ?>">
									<?php echo  $user->data->user_nicename; ?>
								</option>
								<?php }
								?>
							</select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">PO Number: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input required type="text" name="po_number"   />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Field: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input  type="text" name="field"  />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Invoice Number: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input  type="text" name="invoice_number"  />
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Invoice Status: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <select name = "invoice_status">
								<option value = "paid">
									Paid
								</option>
								<option value = "pending">
									Pending
								</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Supplier: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input  type="text" name="supplier_name"  />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Description: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input  type="text" name="description"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Hazardous: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input  type="text" name="hazardous"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Origin: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input  type="text" name="origin"  />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Terms: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input  type="text" name="terms"  />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Method: </div>
                        <div class="col-md-10" style="margin-top:1em;" class="form-group">
                            <select name = "method" id = "choice_you" onchange = "show_form()">
                                <option value = "sea">
                                    Sea
                                </option>
                                <option value = "air">
                                    Air
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="row" style="clear:left;">
                        <div class="col-md-2" style="margin-top:1em;">Number Of Packages: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input  type="text" name="no_of_pkg" />
                        </div>
                    </div>
                    <div class="row" style="clear:left;">
                        <div class="col-md-2" style="margin-top:1em;">Weight(KG): </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input  type="text" name="weight"  />
                        </div>
                    </div>
                    <div class="row" style="clear:left;">
                        <div class="col-md-2" style="margin-top:1em;">Volume(CBM): </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input  type="text" name="volume"  />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Charge Weight(F/T): </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input type="text" name="charge_weight" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;clear:left;">Pick-up request: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input placeholder = "Enter Date" type = "text" name="pickup_req"  class = "tcal" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;clear:left;">Instruct request: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input placeholder = "Enter Date" type = "text"  class="tcal" name = "instruc_req" style="width:15em;" />
                        </div>
                    </div>
                    <div class="row" style="clear:left;">
                        <div class="col-md-2" style="margin-top:1em;">PO Receipt </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input placeholder = "Enter Date" type = "text" name="po_receipt" value = "" class = "tcal" />
                        </div>
                    </div>
                    <div class="row" style="clear:left;">
                        <div class="col-md-2" style="margin-top:1em;">Actioned: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input placeholder = "Enter Date" type = "text" name="actioned" value = "" class = "tcal" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Available: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input placeholder = "Enter Date" type = "text" name="available" value = "" class = "tcal" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:1em;">Collection: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input placeholder = "Enter Date" type = "text" name="collection" value = "" class = "tcal"/>
                        </div>
                    </div>
					<div class="row" style="clear:left;">
                        <div class="col-md-2" style="margin-top:1em;">Required Delivery Date: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input placeholder = "Enter Date" class = "tcal" type = "text" name="req_delivery_date" value = "" />
                        </div>
                    </div>
					<div class="row" style="clear:left;">
                        <div class="col-md-2" style="margin-top:1em;">Actual Delivery Date: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input placeholder = "Enter Date" class = "tcal" type = "text" name="act_delivery_date"  />
                        </div>
                    </div>
						<script>
							function show_form(){
								var x = jQuery("#choice_you").val();
								if(x == 'sea'){
									jQuery("#aspk_air_freight").hide();
									jQuery("#aspk_sea_freight").show();
								}
								if(x == 'air'){
									jQuery("#aspk_sea_freight").hide();
									jQuery("#aspk_air_freight").show();
								}
							}
						</script>
					<div  style="clear:left;border: 1px solid grey; border-radius: 25px;width:44em;height:50em;padding: 1em;margin-top:5em;" id="aspk_sea_freight">
						<div style="clear:left;">
							<h2>Sea Freight</h2>
						</div>
						<div class="row">
							<div class="col-md-4" style="margin-top:1em;">Ship: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input  type = "text" name="ship" value = ""   />
							</div>
						</div>
						<div class="row">
							<div class="col-md-4" style="margin-top:1em;">B/L: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input  type = "text" name="b_l" value = "" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-4" style="margin-top:1em;">Voyage: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input  type = "text" name="voyage" value = "" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-4" style="margin-top:1em;">Port Of Loading: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input  type = "text" name="pol" value = ""   />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">ETD: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input placeholder = "Enter Date" type = "text" name="eta" value = "" class = "tcal" pattern = "^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">ETS: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input  placeholder = "Enter Date" type = "text" name="ets" value = "" class = "tcal" pattern = "^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">ATD: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input placeholder = "Enter Date" type = "text" name="atd" value = "" class = "tcal" pattern = "^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$" />
							</div>
						</div>
						
						 <div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">ETA: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input placeholder = "Enter Date" type = "text" name="n_eta" value = "" class = "tcal" pattern = "^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$" />
							</div>
						</div>
						 <div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">ATA: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input placeholder = "Enter Date" type = "text" name="n_ata" value = "" class = "tcal" pattern = "^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$" />
							</div>
						</div>
						
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Pre Alert: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input placeholder = "Enter Date" type = "text" name="pre_alert" value = "" class = "tcal"  />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Sea Line: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input  type="text" name="sea_line" value = "" />
							</div>
						</div>
						
						 <div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Green Light requested: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="n_g_l_r" value = "" class = "tcal"  />
							</div>
						</div>
						
						 <div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Green light approved: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="n_g_l_a" value = "" class = "tcal" />
							</div>
						</div>
						
						 <div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Number of days for Green Light: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_days_light" value = "" "" />
							</div>
						</div>
					</div>
					<div  style="clear:left;border: 1px solid grey; border-radius: 25px;width:44em;height:42em;padding: 1em;margin-top:5em;display:none;" id="aspk_air_freight">
						<div style="float:left;clear:left;">
							<h2>Air Freight Section:</h2>
						</div>
						 <div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">MAWB: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_mawb"    />
							</div>
						</div>
						 <div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">HAWB: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_hawb"    />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Airport: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_airport"    />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">ETD: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="n_freight_etd"  class = "tcal"  />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">ATD: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="n_freight_atd"  class = "tcal"  />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">ETA: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="n_freight_eta" value = "" class = "tcal" " />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Pre-Alert: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="n_freight_pre_alert" value = "" class = "tcal" pattern = "^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Airline: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_airline" value = ""   />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Green light requested: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="n_freight_green_r" value = "" class = "tcal" pattern = "^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Green Light approved: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="n_freight_green_a" value = "" class = "tcal" pattern = "^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$" />
							</div>
						</div>
					    <div class="row">
							<div class="col-md-4" style="margin-top:1em;">Number of days for green light: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input  type="text" name="n_freight_days_light" value = "" />
							</div>
						</div>
					</div>
					<div  style="clear:left;border: 1px solid grey; border-radius: 25px;width:44em;height:25em;padding: 1em;margin-top:2em;" id="aspk_custom_section">
						<div style="float:left;clear:left;">
							<h2>Customs Section:</h2>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-3" style="margin-top:1em;">Application For Pre Release: </div>
							<div class="col-md-9" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="app_pre_release" value = "" class = "tcal" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-3" style="margin-top:1em;">Pre Release approved: </div>
							<div class="col-md-9" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="pre_release_approved" value = "" class = "tcal"  />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-3" style="margin-top:1em;">Customs Clearance: </div>
							<div class="col-md-9" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="custom_clearance" value = "" class = "tcal" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-3" style="margin-top:1em;">Delivery: </div>
							<div class="col-md-9" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="delivery" value = "" class = "tcal"  />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-3" style="margin-top:1em;">Delivery Note Number: </div>
							<div class="col-md-9" style="margin-top:1em;">
							   <input  type = "text" name="delivery_note_no" value = ""  />
							</div>
						</div>
						 
					</div>
					<div class = "row" style = "margin-top:2em;">
						<div class = "col-md-12"></div>
					</div>
					<?php echo wp_multi_file_uploader();?>
						<div class="row" style="clear:left;">
							
							<div class="col-md-12" style="margin-top:2em;">
								<input type = "submit" name="submit_btn" value = "Save" class = "btn-primary"/>
							</div>
						</div>
				</form>		
            </div>
			
		<?php
		}
		
		function __construct() {
		
			register_activation_hook( __FILE__, array($this, 'install') );
			register_deactivation_hook( __FILE__, array($this, 'de_activate') );
			add_action( 'admin_menu', array(&$this, 'addAdminPage') );
			add_action( 'init', array(&$this, 'front_end') );
			add_shortcode( 'tracking_application', array(&$this, 'tracking_application') );
			add_action('admin_enqueue_scripts', array(&$this, 'backend_scripts') );
			//add_action('wp_enqueue_scripts', array(&$this, 'front_scripts'),999 );
			add_filter( 'login_redirect', array(&$this, 'my_login_redirect'), 10, 3 );
			add_action('wp_logout', array(&$this,'logout_redirect'));
			add_action('wp_head', array(&$this,'include_css'),99);
			add_action( 'wp_ajax_delete', array(&$this,'remove_project'));
			add_action( 'admin_footer', array(&$this,'redirect_from' ));
		} 
		
		function redirect_from(){		
			?><script>	
				//jQuery(document).ready(function (){
				setTimeout(function(){window.location.href= '<?php echo  home_url('wp-login.php');?>'}, 1800000);
				// });	
			</script><?php
		}
			
		function remove_project (){
			
			$id= $_POST['del_id'];
			$po_number = $this->select_project_status_po_number($id);
			delete_post_meta($po_number , _po_cont_dwnld, 0 );
			$this->delete_project($id);
		} 
		
		function Create_Project(){
			?>
			<script>
			   jQuery(document).ready(function (){
					setTimeout(function(){window.location.href= '<?php echo  home_url('wp-login.php');?>'}, 1800000);
				});	
			</script>
			
			<head>
				<style>
					#p:hover {
						background-color: blue;
						font-size: 10px;
						font-weight: 300;
						color:white;
					}
				</style>
			</head>
			<?php
			$link = admin_url();
			?>
			<div class="tw-bs container">	
				<div style = "float:left; clear:left; border-top: 1px solid #e3e3e3; border-bottom: 1px solid #e3e3e3; margin-top:10px;margin-bottom:1em;">
					<ul>
						<a href = "<?php echo $link.'admin.php?page=aspk_product_tracking';?>" style="text-decoration:none;"> 
							<li id = "p" style = "width:14em; float:left; padding:8px;font-size: 14px;"> <b>Manage Client Access </b></li>
						</a>
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b> Create Project </b></li>
							<a href = "<? echo $link.'admin.php?page=aspk_tracking_update_mu'; ?>" style="text-decoration:none;"> 
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b> Update Project Status </b></li>
							</a>
						<a href = "<?php echo $link.'admin.php?page=aspk_View_Performance';?>" style="text-decoration:none;"> 
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b>View Performance </b></li>
						</a>
					</ul>
				</div>
			</div>
			<?
			$this->create_project_tab_form();
		}
		function select_project_status_data($po_no){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}joe_eboje WHERE po_number = '{$po_no}'";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		
		function select_project_status_po_number($id){
			global $wpdb;
			
			$sql = "SELECT po_number FROM {$wpdb->prefix}joe_eboje WHERE id = '{$id}'";
			$rs = $wpdb->get_var($sql);
			return $rs;
		}
		
		function select_project_status_data_for_client($client_name){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}joe_eboje WHERE client_name = '{$client_name}'";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		public function po_number_update(){
			global $wpdb;
			//1:ALTER TABLE table_name
            //DROP COLUMN column_name
			
			// 2:DELETE FROM BlogArticles
				//WHERE Article_ID < 15;
				//GO
				//SELECT * FROM BlogArticles;
				
				//3:DELETE FROM Customers
				//WHERE CustomerName='Alfreds Futterkiste' AND ContactName='Maria Anders';
			if(isset($_POST['get_po_no'])){
				$po_no = $_POST['po_no'];
				
				$result = $this->select_project_status_data($po_no);
				if($result){
					$this->update_project_status_form($result);
				}else{
					?>
						<script>alert('Record Not Found');</script>
					<?php
				}
			}
			//
			if(isset( $_POST['update_btn'] ) ){
				$client_name = $_POST['client_name'];
				$field = $_POST['field'];
				$po_number = $_POST['po_number'];
				$invoice_number = $_POST['invoice_number'];
				$invoice_status = $_POST['invoice_status'];
				$supplier_name = $_POST['supplier_name'];
				$description = $_POST['description'];
				$hazardous = $_POST['hazardous'];
				$origin = $_POST['origin'];
				$terms = $_POST['terms'];
				$method = $_POST['method'];
				$no_of_pkg = $_POST['no_of_pkg'];
				$weight = $_POST['weight'];
				$volume = $_POST['volume'];
				$charge_weight = $_POST['charge_weight'];
				$pickup_req = $_POST['pickup_req'];
				if($pickup_req){
					$pickup_req = new DateTime($pickup_req);
					$pickup_req = $pickup_req->format('Y-m-d');
				}
				$instruc_req = $_POST['instruc_req'];
				   if($instruc_req){
					  $instruc_req = new DateTime($instruc_req);
					  $instruc_req =$instruc_req->format('Y-m-d');
				    }
				$po_receipt = $_POST['po_receipt'];
					if(	$po_receipt){
						$po_receipt = new DateTime($po_receipt);
						$po_receipt = $po_receipt->format('Y-m-d');
				    }
				$actioned = $_POST['actioned'];
					if($actioned){
					  $actioned= new DateTime($actioned);
					  $actioned = $actioned->format('Y-m-d');
				    }
				$available = $_POST['available'];
					if($available){
					   $available = new DateTime($available);
					   $available = $available->format('Y-m-d');
				    }
				$collection = $_POST['collection'];
					if($collection){
					   $collection = new DateTime($collection);
					   $collection = $collection->format('Y-m-d');
				    }
				$ship = $_POST['ship'];
				    if($ship){
					   $ship = new DateTime($ship);
					   $ship = $ship->format('Y-m-d');
				    }
				$b_l = $_POST['b_l'];
				    if($b_l){
					   $b_l = new DateTime($b_l);
					   $b_l = $b_l->format('Y-m-d');
				    }
				$voyage = $_POST['voyage'];
				    if($voyage){
					   $voyage =  new DateTime($voyage);
					   $voyage = $voyage->format('Y-m-d');
				    }
				$pol  =$_POST['pol'];
				    if($pol){
					   $pol  = new DateTime($pol );
					   $pol  = $pol->format('Y-m-d');
				    }
				$eta = $_POST['eta'];
				    if($eta ){
					   $eta  = new DateTime($eta );
					   $eta  = $eta->format('Y-m-d');
				    }
				$feta =$_POST['feta_number'];
				    if($feta){
					   $feta   = new DateTime($feta);
					    $feta  = $feta->format('Y-m-d');
				    }
				$ets = $_POST['ets'];
				    if($ets){
					   $ets  = new DateTime($ets);
					   $ets  = $ets->format('Y-m-d');
			     	}
				$atd = $_POST['atd'];
				    if($atd ){
					   $atd  = new DateTime($atd);
					    $atd = $atd->format('Y-m-d');
				    }
				$pre_alert = $_POST['pre_alert'];
			     	if($pre_alert ){
					   $pre_alert = new DateTime($pre_alert);
					   $pre_alert = $pre_alert->format('Y-m-d');
				    }
				$sea_line = $_POST['sea_line'];
		        	/*if($sea_line){
					   $sea_line = new DateTime($sea_line);
					   $sea_line = $sea_line->format('Y-m-d');
				    }*/
				$sea_n_ata = $_POST['n_ata']; 
					if($sea_n_ata ){
					   $sea_n_ata = new DateTime($sea_n_ata );
					   $sea_n_ata = $sea_n_ata->format('Y-m-d');
				    }	
				$sea_n_g_l_r = $_POST['n_g_l_r'];
			    	if($sea_n_g_l_r ){
					   $sea_n_g_l_r = new DateTime($sea_n_g_l_r);
					   $sea_n_g_l_r = $sea_n_g_l_r->format('Y-m-d');
				    }				
				$sea_n_g_l_a = $_POST['n_g_l_a'];
				    if($sea_n_g_l_a ){
					   $sea_n_g_l_a = new DateTime($sea_n_g_l_a);
					   $sea_n_g_l_a = $sea_n_g_l_a->format('Y-m-d');
				    }								
				$sea_n_days_light = $_POST['n_days_light'];
				$n_mawb = $_POST['n_mawb'];
				$n_hawb = $_POST['n_hawb'];
				$n_airport = $_POST['n_airport'];
				$n_freight_etd = $_POST['n_freight_etd'];
				    if($n_freight_etd){
					   $n_freight_etd = new DateTime($n_freight_etd);
					   $n_freight_etd = $n_freight_etd->format('Y-m-d');
				    }				
				$n_freight_atd = $_POST['n_freight_atd'];
				    if($n_freight_atd){
					   $n_freight_atd = new DateTime($n_freight_atd);
					   $n_freight_atd = $n_freight_atd->format('Y-m-d');
				    }					
				$n_freight_eta = $_POST['n_freight_eta'];
			        if($n_freight_eta){
					   $n_freight_eta = new DateTime($n_freight_eta);
					   $n_freight_eta = $n_freight_eta->format('Y-m-d');
					}
				$n_freight_pre_alert = $_POST['n_freight_pre_alert'];
				   if($n_freight_pre_alert){
					   $n_freight_pre_alert = new DateTime($n_freight_pre_alert);
					   $n_freight_pre_alert = $n_freight_pre_alert->format('Y-m-d');
					}
				$n_airline = $_POST['n_airline'];
				$n_freight_green_r = $_POST['n_freight_green_r'];
				    if($n_freight_green_r){
					   $n_freight_green_r = new DateTime($n_freight_pre_alert);
					   $n_freight_green_r = $n_freight_green_r->format('Y-m-d');
					}
				$n_freight_green_a = $_POST['n_freight_green_a'];
				    if($n_freight_green_a){
					    $n_freight_green_a = new DateTime( $n_freight_green_a);
					    $n_freight_green_a = $n_freight_green_a->format('Y-m-d');
					}
				$n_freight_days_light  = $_POST['n_freight_days_light'];
				
				$application_for_pre_release = $_POST['app_pre_release'];
                    if($application_for_pre_release){
					    $application_for_pre_release = new DateTime($application_for_pre_release);
					    $application_for_pre_release = $application_for_pre_release->format('Y-m-d');
					}				
				$pre_release_approval = $_POST['pre_release_approved'];
				    if($pre_release_approval){
					    $pre_release_approval = new DateTime($pre_release_approval);
					    $pre_release_approval= $pre_release_approval->format('Y-m-d');
					}
				$custom_clearance = $_POST['custom_clearance'];
			        if(	$custom_clearance){
					  	$custom_clearance= new DateTime($custom_clearance);
					  	$custom_clearance= $custom_clearance->format('Y-m-d');
					}					
				$delivery =$_POST['delivery'];
				    if($delivery){
					   $delivery = new DateTime($delivery);
					   $delivery = $delivery->format('Y-m-d');
					}					
				$delivery_note_no = $_POST['delivery_note_no'];
				
				$hidden_id = $_POST['hid'];
				$comments = $_POST['comment_txtarea'];
				$req_delivery_date = $_POST['req_delivery_date'];
				    if($req_delivery_date){
					  $req_delivery_date = new DateTime($req_delivery_date);
					  $req_delivery_date = $req_delivery_date->format('Y-m-d');
					}				
				 $act_delivery_date = $_POST['act_delivery_date'];
				    if($act_delivery_date){
					  $act_delivery_date = new DateTime($act_delivery_date);
					  $act_delivery_date = $act_delivery_date->format('Y-m-d');
					}				
				$attach_id = $_POST['att_id'];
				if($attach_id == 'N;' || !$attach_id[0]){
					$attach_id = '';
				}
				if(!$attach_id){
					$attach_id = $_POST['wp_multi_file_uploader_'];
				}	
				$attach_id = serialize( $attach_id );
				$this->update_project_tab( $feta, $po_number, $client_name, $field, $invoice_number, $invoice_status,$supplier_name, $description, $hazardous, $origin, $terms, $method, $no_of_pkg, $weight, $volume, $charge_weight, $pickup_req, $instruc_req, $po_receipt, $actioned, $available, $collection, $ship, $b_l, $voyage, $pol, $eta, $ets, $atd, $pre_alert, $sea_line, $comments, $hidden_id,$sea_n_ata,$sea_n_g_l_r,$sea_n_g_l_a,$sea_n_days_light,$n_mawb,$n_hawb,$n_airport,$n_freight_etd,$n_freight_atd,$n_freight_eta,$n_freight_pre_alert,$n_airline,$n_freight_green_r,$n_freight_green_a,$n_freight_days_light, $application_for_pre_release, $pre_release_approval, $custom_clearance, $delivery, $delivery_note_no, $req_delivery_date,$act_delivery_date ,$attach_id);
			}
			if(isset($_POST['del_img'])){
				$del_id = $_POST['hidden_img_id'];
				$row_id = $_POST['hidden_img_id_db'];
				wp_delete_attachment($del_id); 
				$sql = "UPDATE {$wpdb->prefix}joe_eboje SET `attach_id` = ''WHERE `id` = '{$row_id}'
					";
				$wpdb->query($sql);
				?>
				<script>
					jQuery('#hide_form<?php echo $del_id; ?>').hide();
				</script>
				<?php
			}
			//
			?>	<div style = "margin-top:5em;">
					<form method = "POST" id = "update_form">
						PO Number: <input type = "text" name = "po_no" required/>&nbsp;
						<input type = "submit" required id = "get_po_no" name = "get_po_no" value = "GO" class = "button button-primary"/>
					</form>
					
				</div>
			<?php
				
		}
		
	    function get_verticle_bar_chart(){
		  ?>
		  	<script>
			   jQuery(document).ready(function () {
					setTimeout(function(){window.location.href= '<?php echo  home_url('wp-login.php');?>'}, 1800000);
				});	
			</script>   
		  <?
			if(isset($_GET['report'])){
				if($_GET['report'] == 'pdf'){
					$this->generate_report_pdf_frontend();
					exit;
				}
			}
		}
		
		public function update_project_tab( $feta, $po_number, $client_name, $field, $invoice_number, $invoice_status,$supplier_name, $description, $hazardous, $origin, $terms, $method, $no_of_pkg, $weight, $volume, $charge_weight, $pickup_req, $instruc_req, $po_receipt, $actioned, $available, $collection, $ship, $b_l, $voyage, $pol, $eta, $ets, $atd, $pre_alert, $sea_line, $comments, $hidden_id,$sea_n_ata,$sea_n_g_l_r,$sea_n_g_l_a,$sea_n_days_light,$n_mawb,$n_hawb,$n_airport,$n_freight_etd,$n_freight_atd,$n_freight_eta,$n_freight_pre_alert,$n_airline,$n_freight_green_r,$n_freight_green_a,$n_freight_days_light, $application_for_pre_release, $pre_release_approval, $custom_clearance, $delivery, $delivery_note_no,$req_delivery_date,$act_delivery_date,$attach_id){
			global $wpdb;
			
			$date = date('Y-m-d');
			$sql = "UPDATE {$wpdb->prefix}joe_eboje SET `client_name` = '{$client_name}', 
			`po_number` = '{$po_number}', 
			`field` = '{$field}', 
			`invoice_number` = '{$invoice_number}', 
			`invoice_status` = '{$invoice_status}',
			`supplier_name` = '{$supplier_name}', 
			`description` = '{$description}', 
			`hazardous` = '{$hazardous}', 
			`origin` = '{$origin}', 
			`terms` = '{$terms}', 
			`method` = '{$method}',
			`no_of_pkg` = '{$no_of_pkg}',
			`weight` = '{$weight}', 
			`volume` = '{$volume}', 
			`charge_weight` = '{$charge_weight}', 
			`pickup_req` = '{$pickup_req}', 
			`instruc_req` = '{$instruc_req}', 
			`po_receipt` = '{$po_receipt}', 
			`actioned` = '{$actioned}',
			`available` = '{$available}', 
			`collection` = '{$collection}', 
			`ship` = '{$ship}', 
			`b_l` = '{$b_l}', 
			`voyage` = '{$voyage}',
			`pol` = '{$pol}', 
			`eta` = '{$eta}', 
			`first_eta` = '{$feta}',
			`current_date` = '{$date}', 
			`ets` = '{$ets}', 
			`atd` = '{$atd}', 
			`pre_alert` = '{$pre_alert}', 
			`sea_line` = '{$sea_line}',
			`comments` = '{$comments}',
			`attach_id` = '{$attach_id}',
			`new_eta` = '{$sea_n_eta}',
			`new_ata` = '{$sea_n_ata}',
			`new_green_req` = '{$sea_n_g_l_r}',
			`new_green_aproved` = '{$sea_n_g_l_a}',
			`new_no_days` = '{$sea_n_days_light}',			
			`freight_mawb` = '{$n_mawb}',
			`freight_hawb` = '{$n_hawb}',
			`freight_airport` = '{$n_airport}',
			`freight_etd` = '{$n_freight_etd}',
			`freight_atd` = '{$n_freight_atd}',
			`freight_eta` = '{$n_freight_eta}',
			`freight_prealert` = '{$n_freight_pre_alert}',
			`freight_airline` = '{$n_airline}',
			`freight_green_req` = '{$n_freight_green_r}',
			`freight_light_apr` = '{$n_freight_green_a}',
			`freight_day_green` = '{$n_freight_days_light}',
			`application_for_pre_release` = '{$application_for_pre_release}',
			`pre_release_approved` = '{$pre_release_approval}',
			`custom_clearance` = '{$custom_clearance}',
			`delivery` = '{$delivery}',
			`delivery_note_number` = '{$delivery_note_no}',
			`req_delivery_date` = '{$req_delivery_date}',
			`act_delivery_date` = '{$act_delivery_date}'
			 WHERE `id` = '{$hidden_id}'
			";
			$wpdb->query($sql);	
			$date_time = date("Y-m-d H:i:s");
			$create_user = 'Update Project And PO No #'.$po_number;
			$sql = "insert into {$wpdb->prefix}agile_history_log (date,comments) values('{$date_time}','{$create_user}');";
			$wpdb->query($sql);	
			
		}
		
	    function delete_project($id){
			global $wpdb;
		  
			$sql = "delete FROM `".$wpdb->prefix."joe_eboje` WHERE `id` = {$id}";
			$wpdb->query($sql);	
		}
		
		public function update_project_status_form($result){
		
				?>
	     <script>			
				function del_project(){
					var x = jQuery('#del_id').val();
					var data = {
							'action': 'delete',
							'del_id' : x	
						};
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
						jQuery.post(ajaxurl, data, function(response) {
							window.location.href='<?php echo admin_url('admin.php?page=aspk_tracking_update_mu');?>';
						});
						alert('poject has been deleted');
				}

		 </script>
				<?php
			
			?>
				<h3>Update Project Status Tab</h3>
				<div style="">
					<form method="post" action="">
						<input type = "hidden" id="del_id" name="h_id" value="<?php echo $result[0]->id;?>"/>
						<input type = "button" onclick = "del_project();" name="d_project" class="button button-primary" value = "Delete Project"/>
					</form>
				</div>
			<?php
			
			$attachment = $result[0]->attach_id;
			$attachments = unserialize ( $attachment );
			if(!empty($attachments)){
				foreach($attachments as $attachment_id){
					$img = wp_get_attachment_image_src( $attachment_id );
					$img = $img[0];
					$img_url = wp_get_attachment_url( $attachment_id );
					if($img_url){
					$ext = pathinfo($img, PATHINFO_EXTENSION);
					$ext = strtolower($ext);
					if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif' || $ext == 'png'){
						?>
						<div style = "float:left; clear:left; margin-bottom: 3em;">
						<img src = "<?php echo $img; ?>"/></div>
						<?php
					}else{
					?>
						<div style = "float:left; clear:left; margin-bottom: 3em;"><?php echo $img; ?></div>
					<?php } ?>
					<div style="float:left;margin-left:2em; ">
						<form id="hide_form" method = "post">
						<input type = "hidden" name = "hidden_img_id" value = "<?php echo $attachment_id; ?>" />
						<input type = "hidden" name = "hidden_img_id_db" value = "<?php echo $result[0]->id; ?>" />
						<input type = "submit" name = "del_img" style="height: 17px; width: 18px; background-image:url('<? echo plugins_url( 'img/x.png', __FILE__ );?>')"/>
						</form>
					</div>
					<?php
					}
				}
			}
			?>
			
			
			<div class = "tw-bs container"><!--Update Form-->
				<form method = "POST">
					<div class="row" style="clear:left; ">
						<div class="col-md-2" style="margin-top:1em;">Client Name: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type="text" name="client_name" value = "<?php echo $result[0]->client_name;?>" />
						</div>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-2" style="margin-top:1em;">Field: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type="text"  name="field" value = "<?php  echo $result[0]->field;?>" />
						</div>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-2" style="margin-top:1em;">Invoice Number: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type="text" name="invoice_number" value = "<?php echo $result[0]->invoice_number;?>" />
						</div>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-2" style="margin-top:1em;">Invoice Status: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<select name = "invoice_status">
								<option <?php if($result[0]->invoice_status == 'paid'){echo "selected";} ?>value = "paid">
									Paid
								</option>
								<option <?php if($result[0]->invoice_status == 'pending'){echo "selected";} ?> value = "pending">
									Pending
								</option>
							</select>
						</div>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-2" style="margin-top:1em;">Supplier: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type="text" name="supplier_name" value = "<?php echo $result[0]->supplier_name; ?>" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">Description: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type="text" name="description" value = "<?php echo $result[0]->description;?>" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">Hazardous: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type="text" name="hazardous" value = "<?php echo $result[0]->hazardous;?>" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">Origin: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type="text" name="origin" value = "<?php echo $result[0]->origin; ?>" />
						</div>
					</div>
					<div class = "row">
						<div class = "col-md-12">
							<input type = "hidden" name = "att_id[]" value = "<?php foreach($attachments as $attac){ echo $attac ;} ?>"/>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">Terms: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type="text" name="terms" value = "<?php echo $result[0]->terms;?>" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">Method: </div>
						<div class="col-md-10" style="margin-top:1em;" class="form-group">
							<select name = "method" id = "choice_you_update" onchange = "show_form_update()">
								<option <?php if ($result[0]->method == 'sea'){ echo 'selected = "selected"'; }?> value = "sea">
									Sea
								</option>
								<option <?php if ($result[0]->method == 'air'){ echo 'selected = "selected"'; }?> value = "air">
									Air
								</option>
							</select>
						</div>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-2" style="margin-top:1em;">Number Of Packages: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type="text" name="no_of_pkg" value = "<?php echo $result[0]->no_of_pkg;?>" />
						</div>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-2" style="margin-top:1em;">Weight(KG): </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type="text" name="weight" value = "<?php echo $result[0]->weight; ?>" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">Volume(CBM): </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input  type="text" name="volume" value = "<?php echo $result[0]->volume; ?>" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">Charge Weight(F/T): </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type="text" name="charge_weight" value = "<?php echo $result[0]->charge_weight; ?>" />
						</div>
					</div>
					<?php 
						$date_pickup = $result[0]->pickup_req;
						//$date_pickup = date('m/d/Y', strtotime( $date_pickup ) );
					?>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;clear:left;">Pick-up request: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type = "text" name="pickup_req" value = "<?php if($date_pickup != '0000-00-00'){echo $date_pickup ;}?>" class = "tcal" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;clear:left;">Instruct request: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input class = "tcal"type = "text" name = "instruc_req" style="clear:left;" value = "<?php if($result[0]->instruc_req != '0000-00-00'){echo $result[0]->instruc_req;}?>" />
						</div>
					</div>
					<?php 
						$date_porecipt = $result[0]->po_receipt;
						//$date_porecipt = date('m/d/Y', strtotime( $date_porecipt ) );
					?>
					<div class="row" style="clear:left;">
						<div class="col-md-2" style="margin-top:1em;">PO receipt: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type = "text" name="po_receipt" value = "<?php  if( $date_porecipt != '0000-00-00'){echo $date_porecipt;}?>" class = "tcal" />
						</div>
					</div>
					<?php 
						$date_actioned = $result[0]->actioned;
						//$date_actioned = date('m/d/Y', strtotime( $date_actioned ) );
					?>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">Actioned: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type = "text" name="actioned" value = "<?php if($date_actioned != '0000-00-00'){echo $date_actioned;}?>" class = "tcal" />
						</div>
					</div>
					<?php 
						$date_avail = $result[0]->available;
						//$date_avail = date('m/d/Y', strtotime( $date_avail ) );
					?>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">Available: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type = "text" name="available" value = "<?php if($date_avail != '0000-00-00'){echo $date_avail;} ?>" class = "tcal" />
						</div>
					</div>
					<?php $date_collect = $result[0]->collection;?>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;">Collection: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<input type = "text" name="collection" value = "<?php if($date_collect != '0000-00-00'){echo $date_collect;} ?>" class = "tcal"/>
						</div>
					</div>
					<div class="row" style="clear:left;">
                        <div class="col-md-2" style="margin-top:1em;">Required Delivery Date: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input  class = "tcal" type = "text" name="req_delivery_date" value = "<?php if($result[0]->req_delivery_date != '0000-00-00'){echo $result[0]->req_delivery_date;} ?>" />
                        </div>
                    </div>
					<div class="row" style="clear:left;">
                        <div class="col-md-2" style="margin-top:1em;">Actual Delivery Date: </div>
                        <div class="col-md-10" style="margin-top:1em;">
                            <input class = "tcal" type = "text" name="act_delivery_date" value = "<?php if($result[0]->act_delivery_date != '0000-00-00'){echo $result[0]->act_delivery_date;} ?>" />
                        </div>
                    </div>
					<?php 
						$date_ship = $result[0]->ship;
						//$date_ship = date('m/d/Y', strtotime( $date_ship ) );
					?>
						<script>
								jQuery( document ).ready(function() {
									if('air' == '<?php echo $result[0]->method; ?>'){
										jQuery("#aspk_sea_freight_update").hide();
										jQuery("#aspk_air_freight_update").show();
									}
									if('sea' == '<?php echo $result[0]->method; ?>'){
										jQuery("#aspk_sea_freight_update").show();
										jQuery("#aspk_air_freight_update").hide();
									}
								});
								
								function show_form_update(){
									var a = jQuery('#choice_you_update').val();
									if(a == "sea"){
										jQuery("#aspk_air_freight_update").hide();
										jQuery("#aspk_sea_freight_update").show();
									}
									if(a == "air"){
										jQuery("#aspk_sea_freight_update").hide();
										jQuery("#aspk_air_freight_update").show();
									}
								}
						</script>
				<div   style="clear:left;border: 1px solid grey; border-radius: 25px;width:44em;height:50em;padding: 1em;margin-top:5em;" id="aspk_sea_freight_update">
					<div style="clear:left;float:left;">
						<h2>Sea Freight</h2>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-4" style="margin-top:1em;">Ship: </div>
						<div class="col-md-8" style="margin-top:1em;">
							<input  type = "text" name="ship" value = "<?php if($date_ship != '0000-00-00'){echo $date_ship;}?>" />
						</div>
					</div>
					<?php 
						$date_bl = $result[0]->b_l;
						//$date_bl = date('m/d/Y', strtotime( $date_bl ) );
					?>
					<div class="row" style="clear:left;">
						<div class="col-md-4" style="margin-top:1em;">B/L: </div>
						<div class="col-md-8" style="margin-top:1em;">
							<input  type = "text" name="b_l" value = "<?php if($date_bl != '0000-00-00'){echo $date_bl;}?>" />
						</div>
					</div>
					<?php 
						$date_vov = $result[0]->voyage;
						//$date_vov = date('m/d/Y', strtotime( $date_vov  ) );
					?>
					<div class="row" style="clear:left;">
						<div class="col-md-4" style="margin-top:1em;">Voyage: </div>
						<div class="col-md-8" style="margin-top:1em;">
							<input  type = "text" name="voyage" value = "<?php if($date_vov != '0000-00-00'){echo $date_vov;}?>" />
						</div>
					</div>
					<?php 
						$date_pol = $result[0]->pol;
						//$date_pol = date('m/d/Y', strtotime( $date_pol  ) );
					?>
					<div class="row" style="clear:left;">
						<div class="col-md-4" style="margin-top:1em;">Port Of Loading: </div>
						<div class="col-md-8" style="margin-top:1em;">
							
							<input type = "text" name="pol" value = "<?php if($date_pol != '0000-00-00'){echo $date_pol;} ?>" class = "tcal" />
						</div>
					</div>
					<?php 
						$date_eta = $result[0]->eta;
						//$date_eta = date('m/d/Y', strtotime( $date_eta  ) );
					?>
					<div class="row" style="clear:left;">
						<div class="col-md-4" style="margin-top:1em;">ETD: </div>
						<div class="col-md-8" style="margin-top:1em;">
							<input type = "text" name="eta" value = "<?php if($date_eta != '0000-00-00'){echo $date_eta;}?>" class = "tcal" />
						</div>
					</div>
					<?php 
						/* $date_ets = new DateTime($result[0]->ets);
						$date_ets = $date_ets->format('Y-m-d');  */
						
					?>
					<div class="row" style="clear:left;">
						<div class="col-md-4" style="margin-top:1em;">ETS: </div>
						<div class="col-md-8" style="margin-top:1em;">
							<input type = "text" name="ets" value = "<?php if($result[0]->ets != '0000-00-00'){echo $result[0]->ets;}?>" class = "tcal" />
						</div>
					</div>
					<?php 
						$date_atd = $result[0]->atd;
						//$date_atd = date('m/d/Y', strtotime( $date_atd  ) );
					?>
					<div class="row" style="clear:left;">
						<div class="col-md-4" style="margin-top:1em;">ATD: </div>
						<div class="col-md-8" style="margin-top:1em;">
							<input type = "text" name="atd" value = "<?php if($date_atd != '0000-00-00'){echo $date_atd;} ?>" class = "tcal" />
						</div>
					</div>
					<div class="row" style="clear:left;">
                        <div class="col-md-4" style="margin-top:1em;">ETA: </div>
                        <div class="col-md-8" style="margin-top:1em;">
                            <input  type = "text" name="n_eta" value = "<?php if($result[0]->eta != '0000-00-00'){echo $result[0]->eta;} ?>" class = "tcal" />
                        </div>
                    </div>
					 <div class="row" style="clear:left;">
                        <div class="col-md-4" style="margin-top:1em;">ATA: </div>
                        <div class="col-md-8" style="margin-top:1em;">
                            <input  type = "text" name="n_ata" value = "<?php if($result[0]->new_ata!= '0000-00-00'){echo $result[0]->new_ata;} ?>" class = "tcal" />
                        </div>
                    </div>
					<?php 
						$date_pre = $result[0]->pre_alert;
						//$date_pre = date('m/d/Y', strtotime( $date_pre  ) );
					?>
					<div class="row" style="clear:left;">
						<div class="col-md-4" style="margin-top:1em;">Pre Alert: </div>
						<div class="col-md-8" style="margin-top:1em;">
							<input type = "text" name="pre_alert" value = "<?php if($date_pre != '0000-00-00'){echo $date_pre;}?>" class = "tcal" />
						</div>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-4" style="margin-top:1em;">Sea Line: </div>
						<div class="col-md-8" style="margin-top:1em;">
							<input type="text" name="sea_line" value = "<?php echo $result[0]->sea_line;?>" />
						</div>
					</div>
					 <div class="row" style="clear:left;">
                        <div class="col-md-4" style="margin-top:1em;">Green Light requested: </div>
                        <div class="col-md-8" style="margin-top:1em;">
                           <input  type = "text" name="n_g_l_r" value = "<?php if($result[0]->new_green_req != '0000-00-00'){echo $result[0]->new_green_req;}?>" class = "tcal" />
                        </div>
                    </div>
					
					 <div class="row" style="clear:left;">
                        <div class="col-md-4" style="margin-top:1em;">Green light approved: </div>
                        <div class="col-md-8" style="margin-top:1em;">
                           <input  type = "text" name="n_g_l_a" value = "<?php if($result[0]->new_green_aproved != '0000-00-00'){echo $result[0]->new_green_aproved;}?>" class = "tcal" />
                        </div>
                    </div>
					
					 <div class="row" style="clear:left;">
                        <div class="col-md-4" style="margin-top:1em;">Number of days for Green Light: </div>
                        <div class="col-md-8" style="margin-top:1em;">
                           <input  type = "text" name="n_days_light" value = "<?php echo $result[0]->new_no_days;?>"  />
                        </div>
                    </div>
				</div>
					
				<div  style="clear:left;border: 1px solid grey; border-radius: 25px;width:44em;height:42em;padding: 1em;margin-top:5em;display:none;" id="aspk_air_freight_update">
					<div style="float:left;clear:left;">
							<h2>Air Freight Section:</h2>
					</div>
						 <div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">MAWB: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_mawb" value = "<?php echo $result[0]->freight_mawb; ?>"/>
							</div>
						</div>
						 <div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">HAWB: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_hawb" value = "<?php echo $result[0]->freight_hawb; ?>"   />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Airport: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_airport" value = "<?php echo $result[0]->freight_airport; ?>"   />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">ETD: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input placeholder = "Enter Date" type = "text" name="n_freight_etd" value = "<?php if($result[0]->freight_etd != '0000-00-00'){echo $result[0]->freight_etd;} ?>" class = "tcal" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">ATD: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input type = "text" name="n_freight_atd" value = "<?php if($result[0]->freight_atd != '0000-00-00'){echo $result[0]->freight_atd;} ?>" class = "tcal" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">ETA: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input type = "text" name="n_freight_eta" value = "<?php if($result[0]->freight_eta != '0000-00-00'){echo $result[0]->freight_eta;} ?>" class = "tcal" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Pre-Alert: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_freight_pre_alert" value = "<?php if($result[0]->freight_prealert != '0000-00-00'){echo $result[0]->freight_prealert;} ?>" class = "tcal" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Airline: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_airline" value = "<?php echo $result[0]->freight_airline; ?>" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Green light requested: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_freight_green_r" value = "<?php if($result[0]->freight_green_req != '0000-00-00'){echo $result[0]->freight_green_req;} ?>" class = "tcal" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">Green Light approved: </div>
							<div class="col-md-8" style="margin-top:1em;">
							   <input  type = "text" name="n_freight_green_a" value = "<?php if($result[0]->freight_light_apr != '0000-00-00'){echo $result[0]->freight_light_apr;} ?>" class = "tcal" />
							</div>
						</div>
					    <div class="row">
							<div class="col-md-4" style="margin-top:1em;">Number of days for green light: </div>
							<div class="col-md-8" style="margin-top:1em;">
								<input  type="text" name="n_freight_days_light" value = "<?php echo $result[0]->freight_day_green;?>" />
							</div>
						</div>
				</div>
					<div class="row">
						<div class="col-md-9" style="margin-top:1em;">
							<input type="hidden" name="hid" value = "<?php echo $result[0]->id;?>" />
							<input type="hidden" name="po_number" value = "<?php echo $result[0]->po_number; ?>" />
							<input type="hidden" name="feta_number" value = "<?php echo $result[0]->first_eta; ?>" />
						</div>
					</div>
					<div  style="clear:left;border: 1px solid grey; border-radius: 25px;width:44em;height:25em;padding: 1em;margin-top:2em;" id="aspk_custom_section_update">
						<div style="float:left;clear:left;">
							<h2>Customs Section :</h2>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-3" style="margin-top:1em;">Application For Pre Release: </div>
							<div class="col-md-9" style="margin-top:1em;">
							   <input  type = "text" name="app_pre_release" value = "<?php if($result[0]->application_for_pre_release != '0000-00-00'){echo $result[0]->application_for_pre_release;} ?>" class = "tcal" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-3" style="margin-top:1em;">Pre Release approved: </div>
							<div class="col-md-9" style="margin-top:1em;">
							   <input  type = "text" name="pre_release_approved" value = "<?php if($result[0]->pre_release_approved != '0000-00-00'){echo $result[0]->pre_release_approved;} ?>" class = "tcal" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-3" style="margin-top:1em;">Customs Clearance: </div>
							<div class="col-md-9" style="margin-top:1em;">
							   <input  type = "text" name="custom_clearance" value = "<?php if($result[0]->custom_clearance != '0000-00-00'){echo $result[0]->custom_clearance;} ?>" class = "tcal"  />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-3" style="margin-top:1em;">Delivery: </div>
							<div class="col-md-9" style="margin-top:1em;">
							   <input  type = "text" name="delivery" value = "<?php if($result[0]->delivery != '0000-00-00') {echo $result[0]->delivery;}?>" class = "tcal" />
							</div>
						</div>
						<div class="row" style="clear:left;">
							<div class="col-md-3" style="margin-top:1em;">Delivery Note Number: </div>
							<div class="col-md-9" style="margin-top:1em;">
							   <input  type = "text" name="delivery_note_no" value = "<?php echo $result[0]->delivery_note_number; ?>"  />
							</div>
						</div>
						 
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-2" style="margin-top:1em;">Add Comments: </div>
						<div class="col-md-10" style="margin-top:1em;">
							<textarea name = "comment_txtarea" style="width:15em;"><?php echo $result[0]->comments; ?></textarea>
						</div>
					</div>
					
					<?php  if(empty($attachments)){
							echo wp_multi_file_uploader();
						   }
					?>
					<div class="row" style="clear:left;">
						<div class="col-md-12" style="margin-top:1em;">
							<input class="button button-primary" type = "submit" name = "update_btn" value = "Update" />
						</div>
					</div>
				</form>
				<script>
					jQuery(document).ready(function(){
						jQuery('#update_form').hide();
					});
				</script>
			</div>
			<?php
		}	
		
		function insert_project_form_data( $client_name, $po_number, $field, $invoice_number, $invoice_status, $supplier_name, $description, $hazardous, $origin, $terms, $method, $no_of_pkg, $weight, $volume, $charge_weight, $pickup_req, $instruc_req, $po_receipt, $actioned, $available, $collection, $ship, $b_l, $voyage, $pol, $eta, $ets, $atd, $pre_alert, $sea_line,$attach_id ,$sea_n_eta,$sea_n_ata,$sea_n_g_l_r,$sea_n_g_l_a,$sea_n_days_light,$n_mawb,$n_hawb,$n_airport,$n_freight_etd,$n_freight_etd,$n_freight_atd,$n_freight_eta,$n_freight_pre_alert,$n_airline,$n_freight_green_r,$n_freight_green_a,$n_freight_days_light,$application_for_pre_release,$pre_release_approval,$custom_clearance,$delivery,$delivery_note_no,$req_delivery_date,$act_delivery_date){
			global $wpdb;
			
			$date = date('Y-m-d');
			$sql = "insert into {$wpdb->prefix}joe_eboje (client_name,po_number,field,invoice_number,invoice_status,supplier_name,description,hazardous,origin,terms,method,no_of_pkg,weight,volume,charge_weight,pickup_req,instruc_req,po_receipt,actioned,available,collection,ship,b_l,voyage,pol,eta,first_eta,`current_date`,ets,atd,pre_alert,sea_line,comments,attach_id,new_eta,new_ata,new_green_req,new_green_aproved,new_no_days,freight_mawb,freight_hawb,freight_airport,freight_etd,freight_atd,freight_eta,freight_prealert,freight_airline,freight_green_req,freight_light_apr,freight_day_green, application_for_pre_release, pre_release_approved, custom_clearance, delivery, delivery_note_number,req_delivery_date,act_delivery_date) values('{$client_name}','{$po_number}','{$field}','{$invoice_number}', '{$invoice_status}','{$supplier_name}','{$description}','{$hazardous}','{$origin}','{$terms}','{$method}','{$no_of_pkg}','{$weight}','{$volume}','{$charge_weight}','{$pickup_req}','{$instruc_req}','{$po_receipt}','{$actioned}','{$available}','{$collection}','{$ship}','{$b_l}','{$voyage}','{$pol}','{$eta}','{$eta}','{$date}','{$ets}','{$atd}','{$pre_alert}','{$sea_line}','{$comments}','{$attach_id}','{$sea_n_eta}','{$sea_n_ata}','{$sea_n_g_l_r}','{$sea_n_g_l_a}','{$sea_n_days_light}','{$n_mawb}','{$n_hawb}','{$n_airport}','{$n_freight_etd}','{$n_freight_atd}','{$n_freight_eta}','{$n_freight_pre_alert}','{$n_airline}','{$n_freight_green_r}','{$n_freight_green_a}','{$n_freight_days_light}','{$application_for_pre_release}','{$pre_release_approval}','{$custom_clearance}','{$delivery}','{$delivery_note_no}','{$req_delivery_date}','{$act_delivery_date}')";
			$wpdb->query($sql);	
			$date_time = date("Y-m-d H:i:s");
			$create_user = 'Create Project And PO No #'.$po_number;
			$sql = "insert into {$wpdb->prefix}agile_history_log (date,comments) values('{$date_time}','{$create_user}');";
			return $wpdb->query($sql);	
		}
		function delete_create_user(){
			?>
			<script>
			   jQuery(document).ready(function () {
					setTimeout(function(){window.location.href= '<?php echo  home_url('wp-login.php');?>'}, 1800000);
				});	
			</script>
			
			<head>
			<style>
				#p:hover {
					background-color: blue;
					font-size: 10px;
					font-weight: 300;
					color:white;
				}
			</style>
			</head>
			<?php
			$link = admin_url();
			?>
			<div class="tw-bs container">
			
				<div style = "float:left; clear:left; border-top: 1px solid #e3e3e3; border-bottom: 1px solid #e3e3e3; margin-top:10px;">
					<ul>
						<a href = "<?php echo $link.'admin.php?page=aspk_product_tracking';?>" style="text-decoration:none;"> 
						 <li id = "p" style = "width:14em; float:left; padding:8px;font-size: 14px;"> <b>Manage Client Access </b></li>
						 </a>
			
						<a href = "<? echo $link.'admin.php?page=aspk_tracking_view_mu'; ?>" style="text-decoration:none;"> 
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b> Create Project </b></li>
						</a>
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b> Update Project Status </b></li>
						<a href = "<?php echo $link.'admin.php?page=aspk_View_Performance';?>" style="text-decoration:none;"> 
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b>View Performance </b></li>
						</a>
					</ul>
				</div>
			</div>
			<?php
				$this->po_number_update();
		}
		function view_all_users(){
			if(isset($_GET['act'])){
				if($_GET['act']=='del'){
					$userid = $_GET['uid'];
					wp_delete_user( $userid);
				}
			}
			//if isset $_GET['act']
			$args = array(
				'blog_id'      => $GLOBALS['blog_id'],
				'role'         => 'joe_eboje',
				'meta_key'     => '',
				'meta_value'   => '',
				'meta_compare' => '',
				'meta_query'   => array(),
				'include'      => array(),
				'exclude'      => array(),
				'orderby'      => 'login',
				'order'        => 'ASC',
				'offset'       => '',
				'search'       => '',
				'number'       => '',
				'count_total'  => false,
				'fields'       => 'all',
				'who'          => ''
			 );
			$get_all_users = get_users( $args );
				foreach($get_all_users as $k=>$user){
					$user_name = $user->data->user_login;
					$user_email = $user->data->user_email;
					$user_registered = $user->data->user_registered;
					$user_ID = $user->data->ID;
				?>	
					<div class="row" style="clear:left;">
						<div class="col-md-2" style="margin-top:1em;">
							<? echo $user_name;?>
						</div>
						<div class="col-md-3" style="margin-top:1em;">
							<? echo $user_email;?>
						</div>
						<div class="col-md-3" style="margin-top:1em;">
							<? echo $user_registered;?>
						</div>
						<div class="col-md-2" style="margin-top:1em;">
								<input type="hidden" name="h_id" value="<? echo $user_ID; ?>" />
								<?
								$link = admin_url()."?page=aspk_product_tracking&act=del&uid=".$user_ID;
								?>
								<a href="<? echo $link; ?>">
								<img src="<? echo plugins_url( 'img/x.png', __FILE__ );?>"></a>
							
						</div>
						<?php
							$link_pasw = admin_url()."admin.php?page=aspk_change_password&uid=".$user_ID;
						?>
						<div class="col-md-2" style="margin-top:1em;">
							<a href="<? echo $link_pasw; ?>">Change Password</a>
						</div>
					</div>
					
				<?
				}
		}
		function test_tracking_aspk(){
			?>
			<script>
			   jQuery(document).ready(function () {
					setTimeout(function(){window.location.href= '<?php echo  home_url('wp-login.php');?>'}, 1800000);
				});	
			</script>
			<head>
			<style>
				#p:hover {
					background-color: blue;
					font-size: 10px;
					font-weight: 300;
					color:white;
				}
			</style>
			</head>
			<?php
		
			$link = admin_url();
			?>
			<div class="tw-bs container">	
				<div style = "float:left; clear:left; border-top: 1px solid #e3e3e3; border-bottom: 1px solid #e3e3e3; margin-top:10px;">
					<ul>
						 <li id = "p" style = "width:14em; float:left; padding:8px;font-size: 14px;"> <b>Manage Client Access </b></li>
						<a href = "<? echo $link.'admin.php?page=aspk_tracking_view_mu'; ?>" style="text-decoration:none;"> 
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b> Create Project </b></li>
						</a>
						<a href = "<?php echo $link.'admin.php?page=aspk_tracking_update_mu';?>" style="text-decoration:none;"> 
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b> Update Project Status </b></li>
						</a>
						<a href = "<?php echo $link.'admin.php?page=aspk_View_Performance';?>" style="text-decoration:none;"> 
							<li id = "p" style = "width:14em; float:left;padding:8px;font-size: 14px;"><b>View Performance </b></li>
						</a>
					</ul>
				</div><br/>

			<?
			if(isset($_POST['submit'])){
				global $wpdb;
			
				$user_name = $_POST['name123'];
				$user_email = $_POST['e_mail'];
				$random_password = $_POST['password'];
				
				$user_id = username_exists( $user_name );
				if ( !$user_id and email_exists($user_email) == false ) {
					$user_id = wp_create_user( $user_name, $random_password, $user_email );
					wp_update_user( array ('ID' => $user_id, 'role' => 'joe_eboje' ) ) ;
					$date_time = date("Y-m-d H:i:s");
					$create_user = 'Create User and Name is '.$user_name; 
					$sql = "insert into {$wpdb->prefix}agile_history_log (date,comments) values('{$date_time}','{$create_user}');";
					$wpdb->query($sql);	
					?>
						<div style = "margin-top:3em;"class="alert alert-success" role="alert"><b>User Has Been Created Successfully</b></div>
					<?php
				} else {
					$random_password = __('User already exists.  Password inherited.');
					echo "<b>User already exists</b>";
				}	 
			}
				?>
				<form method="post" action="">
					<div class="row" style="clear:left;">
						<div class="col-md-1" style="margin-top:1em;">Name: </div>
						<div class="col-md-11" style="margin-top:1em;">
							<input type="text" name="name123" value = "" required />
						</div>
					</div>
					<div class="row">
						<div class="col-md-1" style="margin-top:1em;">Email: </div>
						<div class="col-md-11" style="margin-top:1em;">
							<input type="email" name="e_mail" value = "" required />
						</div>
					</div>
					<div class="row">
						<div class="col-md-1" style="margin-top:1em;">Password: </div>
						<div class="col-md-11" style="margin-top:1em;">
							<input type="password" name="password" value = "" required />
						</div>
					</div>
					<div class="row">
						<div class="col-md-1" style="margin-top:1em;"></div>
						<div class="col-md-11" style="margin-top:1em;">
							<input type="submit" name="submit" value = "Create User" class="btn-primary"/>
						</div>
					</div>
				</form>
					<div class="row">
						<div class="col-md-2" style="margin-top:2em;">
							<b>Name</b>
						</div>
						<div class="col-md-3" style="margin-top:2em;">
							<b>Email</b>
						</div>
						<div class="col-md-3" style="margin-top:2em;">
							<b>Date of Creation</b>
						</div>
						<div class="col-md-2" style="margin-top:2em;">
							<b>Delete User</b>
						</div>
						<div class="col-md-2" style="margin-top:2em;">
							<b>Change Password</b>
						</div>
					</div>
				<?php
					$this->view_all_users();	
			?>
			</div>
			<?php
		}
		function de_activate(){
			remove_role( 'joe_eboje' );
		}
		function rand_string( $length ) {
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			return substr(str_shuffle($chars),0,$length);
		}
	}//class ends
}//existing class ends
$aspk_track = new Agile_Project_tracking_application();