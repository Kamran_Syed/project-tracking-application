<?php
	/* Libchart - PHP chart library
	 * Copyright (C) 2005-2011 Jean-Marc Tr�meaux (jm.tremeaux at gmail.com)
	 * 
	 * This program is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
	 * 
	 */
	
	/*
	 * Vertical bar chart demonstration
	 *
	 /
	/*Air Freight forwarding Graph*/
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	require_once ABSPATH ."/wp-content/plugins/project-tracking/libchart/classes/libchart.php";
	$chart = new VerticalBarChart();
	$dataSet = new XYDataSet();
	foreach($result as $r){
			if($r->collection == '0000-00-00' || $r->freight_atd == '0000-00-00') continue;
				$date1=date_create($r->collection);
				$date2=date_create($r->freight_atd);
				$diff=date_diff($date1,$date2);	
				$dataSet->addPoint(new Point( $r->po_number ,$diff->days));
	}
	$chart->setDataSet($dataSet);

	$chart->setTitle("Air Freight forwarding");
	$chart->render(__DIR__ ."/generated/demo1.png");
	$url = home_url().'/wp-content/plugins/project-tracking/libchart/demo/generated/demo1.png';
?>
	<img alt="Vertical bars chart" src="<?php echo $url;?>" style="border: 1px solid lightgray; margin-top:2em;"/>
<?php

       /*Air Freight Custom clearance Graph*/
   $parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	require_once ABSPATH ."/wp-content/plugins/project-tracking/libchart/classes/libchart.php";
	$chart = new VerticalBarChart();
	$dataSet = new XYDataSet();
    foreach($result as $re){
		if($re->delivery == '0000-00-00' || $re->freight_atd == '0000-00-00') continue;
			$date1=date_create($re->delivery);
			$date2=date_create($re->freight_atd);
			$diff=date_diff($date1,$date2);
			$dataSet->addPoint(new Point( $re->po_number , $diff->days));
	}
	$chart->setDataSet($dataSet);

	$chart->setTitle("Air Freight Custom clearance");
	$chart->render(__DIR__ ."/generated/demo2.png");
	$url = home_url().'/wp-content/plugins/project-tracking/libchart/demo/generated/demo2.png';
?>
	<img alt="Vertical bars chart" src="<?php echo $url;?>" style="border: 1px solid lightgray;"/>
<?php

       /*Sea Freight Forwarding Graph*/
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	require_once ABSPATH ."/wp-content/plugins/project-tracking/libchart/classes/libchart.php";
	$chart = new VerticalBarChart();
	$dataSet = new XYDataSet();
foreach($result as $res){
		if($res->collection == '0000-00-00' || $res->new_ata == '0000-00-00') continue;
			$date1=date_create($res->collection);
			$date2=date_create($res->new_ata);
			$diff=date_diff($date1,$date2);
			$dataSet->addPoint(new Point( $res->po_number , $diff->days));
	}
	$chart->setDataSet($dataSet);

	$chart->setTitle("Sea Freight Forwarding");
	$chart->render(__DIR__ ."/generated/demo3.png");
	$url = home_url().'/wp-content/plugins/project-tracking/libchart/demo/generated/demo3.png';
?>
	<img alt="Vertical bars chart" src="<?php echo $url;?>" style="border: 1px solid lightgray;"/>
<?php

        /*Sea Freight customs clearance Graph*/
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	require_once ABSPATH ."/wp-content/plugins/project-tracking/libchart/classes/libchart.php";
	$chart = new VerticalBarChart();
	$dataSet = new XYDataSet();
foreach($result as $resu){	
		if($resu->delivery == '0000-00-00' || $resu->new_ata == '0000-00-00') continue;
			$date1=date_create($resu->delivery);
			$date2=date_create($resu->new_ata);
			$diff=date_diff($date1,$date2);
			$dataSet->addPoint(new Point( $resu->po_number , $diff->days));
	}
	$chart->setDataSet($dataSet);

	$chart->setTitle("Sea Freight customs clearance");
	$chart->render(__DIR__ ."/generated/demo4.png");
	$url = home_url().'/wp-content/plugins/project-tracking/libchart/demo/generated/demo4.png';
?>
	<img alt="Vertical bars chart" src="<?php echo $url;?>" style="border: 1px solid lightgray;"/>
<?php
